import 'package:flutter/material.dart';
import 'package:midterm_project/student_grade.dart';

String dropdownvalue = 'item1';

class SubjectRegister extends StatelessWidget {
  const SubjectRegister({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBodyStdCarendarWidget(),
    );
  }
}

Widget buildBodyStdCarendarWidget(){
  return Container(
      padding: EdgeInsets.all(6),
    child:  ListView(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ปฏิทินการศึกษา",textScaleFactor: 2.5,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
        ),
        // Text('ปฏิทินการศึกษา //ดู ดาวน์โหลด'),
        Center(
          child: DropdownButton<String>(
            hint: Text('10300 : ปริญญาตรี ปกติ ปี 3', style: TextStyle(color: Colors.brown[900]),),
            items: <String>['...', '10100 : ปริญญาตรี ปกติ ปี 1', '10200 : ปริญญาตรี ปกติ ปี 2', '10300 : ปริญญาตรี ปกติ ปี 3', '10400 : ปริญญาตรี ปกติ ตั้งแต่ ปี 4 ขึ้นไป', '...'].map((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value, style: TextStyle(color: Colors.brown[900]),),
              );
            }).toList(),
            onChanged: (val) {
              // setState(() {
              dropdownvalue = val!;
              // });
            },
          ),
        ),
        buildSelectYearWidget(),
        buildSelectDurationWidget(),
        // Divider(
        //   color: Colors.brown[700],
        // ),
        buildTableCarendar(),
      ]
    )
  );
}

Widget buildSelectYearWidget(){
  return Row(
    children: [
      Text('ปีการศึกษา:'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.arrow_left_rounded,
          )
      ),
      Text('2565', style: TextStyle( color: Colors.blueGrey[600], fontWeight: FontWeight.bold),),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.arrow_right_rounded,
          )
      ),
    ],
  );
}

Widget buildSelectDurationWidget(){
  return Row(
    children: [
      Icon(Icons.more_vert),
      Text('ภาคเรียน:'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.looks_one_outlined,
          )
      ),
      Text('/'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.looks_two_outlined,
            color: Colors.blueGrey,
          )
      ),
      Text('/'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.sunny,
          )
      ),
      Text('ภาคฤดูร้อน'),
    ],
  );
}

buildTableCarendar(){
  return Table(
    columnWidths: const <int, TableColumnWidth>{
      0 : FlexColumnWidth(4),
      1 : FlexColumnWidth(3),
    },
    defaultColumnWidth: const FlexColumnWidth(),
    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
    children: <TableRow>[
      TableRow(
        decoration: BoxDecoration(
          color: Colors.brown[100],
        ),
        children: const <Widget>[
          GraduateChCell(title: 'รายการ', isHeader: true),
          GraduateChCell(title: 'ช่วงเวลา', isHeader: true),
        ],
      ),
      TableRow(
        children: const <Widget>[
          GraduateChCell(title: 'ช่วงจองรายวิชาลงทะเบียนให้กับนิสิตโดยเจ้าหน้าที่',),
          GraduateChCell(title: '1 พ.ย. 2565 8:30 น. -\n18 พ.ย. 2565 23:59 น.',),
        ],
      ),
      // TableRow(
      //   children: const <Widget>[
      //     GraduateChCell(title: 'ลงทะเบียนปกติ ( on-line )', isHeader: true),
      //     GraduateChCell(title: '11 พ.ย. 2565 8:30 น. -\n18 พ.ย. 2565 23:59 น.',),
      //   ],
      // ),
      ...CarendarStd.getRegisDuration().map((carendarStd) {
        return TableRow(
            decoration: BoxDecoration(
              color: Colors.brown[50],
            ),
            children: [
          GraduateChCell(title: carendarStd.listCarendar),
          GraduateChCell(title: carendarStd.duration),
        ]);
      }),
      TableRow(
        children: const <Widget>[
          GraduateChCell(title: 'เริ่มคิดค่าปรับลงทะเบียนล่าช้า',),
          GraduateChCell(title: 'ตั้งแต่ 19 พ.ย. 2565 0:00 น.',),
        ],
      ),
      TableRow(
        children: const <Widget>[
          GraduateChCell(title: 'วันเปิดภาคการศึกษา', isHeader: true),
          GraduateChCell(title: '19 พ.ย. 2565 0:00 น.',),
        ],
      ),
      TableRow(
        children: const <Widget>[
          GraduateChCell(title: 'ช่วงยื่นคำร้องขอขยายเวลาชำระเงิน ( ขาดแคลนทุนทรัพย์อย่างแท้จริง ) ที่คณะ',),
          GraduateChCell(title: '19 พ.ย. 2565 0:00 น. -\n23 ธ.ค. 2565 16:00 น.',),
        ],
      ),
      TableRow(
        children: const <Widget>[
          GraduateChCell(title: 'ช่วงวันทำรายการเทียบโอน',),
          GraduateChCell(title: '19 พ.ย. 2565 0:00 น. -\n2 ธ.ค. 2565 16:30 น.',),
        ],
      ),
      TableRow(
        children: const <Widget>[
          GraduateChCell(title: 'ลงทะเบียนเพิ่ม/ลด/เปลี่ยนกลุ่ม ( on-line )', isHeader: true),
          GraduateChCell(title: '21 พ.ย. 2565 8:30 น. -\n28 พ.ย. 2565 23:59 น.',),
        ],
      ),TableRow(
        children: const <Widget>[
          GraduateChCell(title: 'เริ่มคิดค่าปรับเพิ่ม-ลดรายวิชาล่าช้า',),
          GraduateChCell(title: 'ตั้งแต่ 30 พ.ย. 2565 0:00 น.',),
        ],
      ),
      TableRow(
        children: const <Widget>[
          GraduateChCell(title: 'วันสุดท้ายของการลงทะเบียน เพิ่ม-ลดรายวิชาล่าช้า โดยทำคำร้อง',),
          GraduateChCell(title: '6 ธ.ค. 2565 0:00 น.',),
        ],
      ),
      TableRow(
        children: const <Widget>[
          GraduateChCell(title: 'งวดชำระเงินตามปกติ\n'
              '- ช่วงพิมพ์ใบแจ้งหนี้ผ่าน WEB\n'
              '- ช่วงชำระเงินออนไลน์หรือเคาน์เตอร์ธ.กรุงไทย ธ.ไทยพาณิชย์ ธ.กสิกร ธ.ธนชาต ธ.กรุงเทพ ทุกสาขาทั่วประเทศ\n'
              '- ช่วงวันชำระเงินค่ารักษาสภาพออนไลน์', isHeader: true),
          GraduateChCell(title: '15 ธ.ค. 2565 0:00 น. -\n22 ธ.ค. 2565 22:30 น.',),
        ],
      ),
      TableRow(
        children: const <Widget>[
          GraduateChCell(title: 'ช่วงวันทำการงดเรียนรายวิชา ซึ่งจะติด W ในทรานสคริป',),
          GraduateChCell(title: '23 ธ.ค. 2565 0:00 น. -\n24 ก.พ. 2566 16:30 น.',),
        ],
      ),
      TableRow(
        children: const <Widget>[
          GraduateChCell(title: '...',),
          GraduateChCell(title: '...',),
        ],
      ),
    ],
  );
}

class CarendarStd {
  final String listCarendar;
  final String duration;

  CarendarStd(this.listCarendar, this.duration);

  static List<CarendarStd> getRegisDuration() {
    return [
      CarendarStd('ลงทะเบียนปกติ ( on-line ) ชั้นปี 4 / ชั้นปีอื่นๆ', '11 พ.ย. 2565 8:30 น. -\n18 พ.ย. 2565 23:59 น.',),
      CarendarStd('ลงทะเบียนปกติ ( on-line ) ชั้นปี 3', '14 พ.ย. 2565 8:30 น. -\n18 พ.ย. 2565 23:59 น.',),
      CarendarStd('ลงทะเบียนปกติ ( on-line ) ชั้นปี 2', '15 พ.ย. 2565 8:30 น. -\n18 พ.ย. 2565 23:59 น.',),
      CarendarStd('ลงทะเบียนปกติ ( on-line ) ชั้นปี 1', '16 พ.ย. 2565 8:30 น. -\n18 พ.ย. 2565 23:59 น.',),
    ];
  }
}