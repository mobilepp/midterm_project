import 'package:flutter/material.dart';
import 'package:side_navigation/side_navigation.dart';
import 'package:midterm_project/my_flutter_app_icons.dart';
import 'package:midterm_project/subject_regis.dart';
import 'package:midterm_project/login_page.dart';
import 'package:midterm_project/ltr_table.dart';
import 'package:midterm_project/std_calendar.dart';
import 'package:midterm_project/doc_graduation.dart';
import 'package:midterm_project/doc_rela.dart';
import 'package:midterm_project/contact.dart';

class RegF extends StatefulWidget {
  const RegF({Key? key}) : super(key: key);

  @override
  State<RegF> createState() => _RegFState();
}

class _RegFState extends State<RegF> {
  List<Widget> views = [];
  int selectedIndex = 0;
  int toggle = 0;
  @override
  Widget build(BuildContext context) {
    setState(() {
      views = [
        Center(
          child: buildBodyFMainWidget(context), //Text('หน้าหลัก //ปุ่มไปหน้าต่างๆ ที่เพิ่มเติมเข้ามาหลังล็อกอิน'),
        ),
        Center(
          child: buildBodySubjWidget(), //Text('หลักสูตร/วิชาที่เปิดสอน //แนะนำการลงทะเบียน ค้นหา'),
        ),
        Center(
          child: buildBodyTableLTRWidget(), //Text('ตารางเรียน/สอน/ใช้ห้อง'),
        ),
        Center(
          child: buildBodyStdCarendarWidget(), //Text('ปฏิทินการศึกษา //ดู ดาวน์โหลด'),
        ),
        Center(
          child: buildBodyDocGradeWidget(), //Text('การสำเร็จการศึกษา //ผู้สำเร็จการศึกษา ตรวจสอบวันสำเร็จการศึกษา การยื่นขอสำเร็จการศึกษา'),
        ),
        Center(
          child: buildBodyDocRelaWidget(), //Text('เอกสาร/ข้อมูลต่างๆ ที่เกี่ยวข้อง //แบบฟอร์ม ค่าธรรมเนียม ระเบียบข้อบังคับ สถิตินิสิต ข้อมูลรายงานตัว สารสนเทศเจ้าหน้าที่'),
        ),
        Center(
          child: buildBodyContactWidget(), //Text('ติดต่อเรา //ถามในระบบ ช่องทางอื่น'),
        ),
      ];
    });

    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primarySwatch: Colors.brown,
            textTheme: TextTheme(
              bodyText2: TextStyle(color: Colors.brown[900]),
              subtitle1: TextStyle(color: Colors.brown[900]),
            ),
            iconTheme: IconThemeData(
              color: Colors.brown[900],
            )

        ),
        home: Scaffold(
          appBar: buildFAppBar(context),
            body: //buildBodyMainWidget(),
            Row(
              children: [
                SideNavigationBar(
                  footer: SideNavigationBarFooter(
                      label: Text('Expanded Now!')
                  ),
                  selectedIndex: selectedIndex,
                  items: const [
                    SideNavigationBarItem(
                      icon: Icons.home,
                      label: 'หน้าหลัก', //ปุ่มไปหน้าต่างๆ ที่เพิ่มเติมเข้ามาหลังล็อกอิน
                    ),
                    SideNavigationBarItem(
                      icon: Icons.subject,
                      label: 'หลักสูตร/วิชาที่เปิดสอน', //แนะนำการลงทะเบียน ค้นหา
                    ),
                    SideNavigationBarItem(
                      icon: Icons.table_view,
                      label: 'ตารางเรียน/สอน/ใช้ห้อง',
                    ),
                    SideNavigationBarItem(
                      icon: Icons.calendar_month,
                      label: 'ปฏิทินการศึกษา', //ดู ดาวน์โหลด
                    ),
                    SideNavigationBarItem(
                      icon: CustomIcons.graduation_cap_1, //Icons.grade,
                      label: 'เกี่ยวกับการสำเร็จการศึกษา', //ผู้สำเร็จการศึกษา ตรวจสอบวันสำเร็จการศึกษา การยื่นขอสำเร็จการศึกษา
                    ),
                    SideNavigationBarItem(
                      icon: CustomIcons.doc, //Icons.document_scanner,
                      label: 'เอกสาร/ข้อมูลต่างๆ ที่เกี่ยวข้อง', //แบบฟอร์ม ค่าธรรมเนียม ระเบียบข้อบังคับ สถิตินิสิต ข้อมูลรายงานตัว สารสนเทศเจ้าหน้าที่
                    ),
                    SideNavigationBarItem(
                      icon: CustomIcons.contacts, //Icons.contact_support,
                      label: 'ติดต่อเรา', //ถามในระบบ ช่องทางอื่น
                    ),
                  ],
                  onTap: (index) {
                    setState(() {
                      selectedIndex = index;
                    });
                  },
                  theme: SideNavigationBarTheme(
                    backgroundColor: Colors.brown[50],
                    itemTheme: SideNavigationBarItemTheme(
                        unselectedItemColor: Colors.brown[700],
                        selectedItemColor: Colors.blueGrey,
                        iconSize: 32.5,
                        labelTextStyle: TextStyle(
                            fontSize: 20,
                            color: Colors.black
                        )
                    ),
                    togglerTheme: SideNavigationBarTogglerTheme.standard(),
                    dividerTheme: SideNavigationBarDividerTheme.standard(),
                  ),
                  toggler: SideBarToggler(
                      expandIcon: Icons.keyboard_arrow_right,
                      shrinkIcon: Icons.keyboard_arrow_left,
                      onToggle: () {
                        print('Toggle');
                        setState(() {
                          ++toggle;
                        });
                        print(toggle);
                      }),
                ),
                /// Make it take the rest of the available width
                Flexible(
                  // child: toggle % 2 != 0 ? views.elementAt(selectedIndex): Container(),
                  child: views.elementAt(selectedIndex),
                )
              ],
            )
        )
    );
  }
}

AppBar buildFAppBar(BuildContext context){
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.brown[700],
    title: Text("Reg BUU App"),
    actions: <Widget>[
      IconButton(
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const Login()),
            );
          },
          icon: Icon(
            Icons.login_outlined,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.g_translate_outlined,
            color: Colors.white,
          )
      ),
    ],
  );
}

Widget buildBodyFMainWidget(BuildContext context){
  final screenWidth = MediaQuery.of(context).size.width;
  final screenHeight = MediaQuery.of(context).size.height;

  return ListView (
    children: <Widget>[
      Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              LayoutBuilder(builder: (context, constraints) {
                final wideEnough = constraints.maxWidth > 400;
                return Container(
                    width: double.infinity,
                    height: wideEnough ? screenHeight*0.35 : screenHeight*0.2,//170,
                    child: Image.network(
                      "https://images.rawpixel.com/image_png_600/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvdjEwMjAtYmctMDFiLnBuZw.png",
                      fit: BoxFit.cover,
                    )
                );
              }),
              // Container(
              //     width: double.infinity,
              //     height: MediaQuery.of(context).size.height * 0.29,//170,
              //     child: Image.network(
              //       "https://images.rawpixel.com/image_png_600/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvdjEwMjAtYmctMDFiLnBuZw.png",
              //       fit: BoxFit.cover,
              //     )
              // ),
              Positioned(
                // top: 25,
                // left: 70,
                child: Column(
                    children: <Widget>[
                      Center(
                          child: LayoutBuilder(builder: (context, constraints) {
                            final wideEnough = constraints.maxWidth > 1900;
                            return Padding(
                              padding: wideEnough ? EdgeInsets.only(top: screenHeight * 0.035) : EdgeInsets.only(top: screenHeight * 0.045),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.brown.withOpacity(0.5),
                                      spreadRadius: 3.0,
                                      blurRadius: 7.0,
                                      offset: Offset(3.0, 3.0),
                                    )
                                  ],
                                ),
                                child: LayoutBuilder(builder: (context, constraints) {
                                  final wideEnough = constraints.maxWidth > 400;
                                  return Image(
                                    image: AssetImage('images/buu_logo.png'),
                                    height: wideEnough ? screenWidth*0.05 : screenWidth*0.12,
                                    width: wideEnough ? screenWidth*0.05 : screenWidth*0.12,
                                  );
                                }),
                                // Image(
                                //   image: AssetImage('images/buu_logo.png'),
                                //   height: screenWidth*0.05, width: screenWidth*0.05,
                                // ),
                              ),
                            );
                          }),
                      ),
                      Center(
                        child: Text('ระบบบริการการศึกษา' '\nมหาวิทยาลัยบูรพา',
                          style: TextStyle(
                            fontSize: 23, fontWeight: FontWeight.bold, color: Colors.brown[900],
                            shadows: [
                              Shadow(
                                blurRadius: 5.0,
                                color: Colors.brown.shade300,
                                offset: Offset(5.0, 5.0),
                              ),
                            ],
                          ), textAlign: TextAlign.center,),
                      )
                    ]
                ),
              ),
            ],
          ),
          Container(
            height: 50,
            color: Colors.brown[50],
            child: ListTile(
              title: Text('ประกาศ!!!', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.brown[900]),),
              trailing: IconButton(
                  onPressed: (){},
                  icon: Icon(
                    Icons.navigate_next,
                    size: 20,
                    color: Colors.brown[900],
                  )
              ),
            ),
          ),
        ],
      ),
      Divider(
        color: Colors.brown[700],
      ),
      Container(
        height: 150.0,
        child: buildListAdvertise(25, 20),
      ),
      Divider(
        color: Colors.brown[700],
      ),
      Container(
        padding: EdgeInsets.all(10),
          width: 190,
          height: 300,
          child: Image(
            image: AssetImage('images/Line.jpg'),
            fit: BoxFit.cover,
          )
      ),
      Flexible(
        child: Container(
          width: 190,
          child: Text('LINE Official\n'
              'เพิ่มเพื่อน LINE Official ของกองทะเบียนฯ พิมพ์ "@regbuu" (ใส่ @ ด้วย) หรือ https://lin.ee/uNohJQP หรือสแกน Qrcode ด้านล่าง', style: TextStyle(
              fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
        ),
      ),
    ],
  );
}

Widget buildListAdvertise(spacew, sizew){
  // double? spacew = 25;
  // double? sizew = 20;
  double width = 190;
  double height = 120;
  return ListView(
    // This next line does the trick.
    scrollDirection: Axis.horizontal,
    children: <Widget>[
      SizedBox(width: 10),
      Column(
        children: <Widget>[
          Container(
              width: width,
              height: height,
              child: Image(
                image: AssetImage('images/pay652.jpg'),
                fit: BoxFit.cover,
              )
          ),
          Flexible(
            child: Container(
              width: width,
              child: Text('กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565', style: TextStyle(
                  fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ),
          ),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Container(
              width: width,
              height: height,
              child: Image.network("https://images.rawpixel.com/image_600/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvdjkyNi1hZGotMDMxXzJfMS5qcGc.jpg",
                fit: BoxFit.cover,
              )
          ),
          Flexible(
            child: Container(
              width: width,
              child: Text('advertise 2', style: TextStyle(
                  fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ),
          ),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Container(
              width: width,
              height: height,
              child: Image.network("https://images.rawpixel.com/image_600/cHJpdmF0ZS9sci9pbWFnZXMvd2Vic2l0ZS8yMDIyLTA1L3BmLXM5Ny1rdC0wMTYzLTAxLmpwZw.jpg",
                fit: BoxFit.cover,
              )
          ),
          Flexible(
            child: Container(
              width: width,
              child: Text('advertise 3', style: TextStyle(
                  fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ),
          ),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Container(
              width: width,
              height: height,
              child: Image.network("https://t4.ftcdn.net/jpg/04/61/23/23/360_F_461232389_XCYvca9n9P437nm3FrCsEIapG4SrhufP.jpg",
                fit: BoxFit.cover,
              )
          ),
          Flexible(
            child: Container(
              width: width,
              child: Text('advertise 4', style: TextStyle(
                  fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ),
          ),
        ],
      ),
      // Column(
      //   children: <Widget>[
      //     Text('01', style: TextStyle(
      //         fontSize: 11, fontWeight: FontWeight.bold)),
      //     Icon(CustomIcons.cloud_moon, color: Colors.indigoAccent, size: sizew,),
      //     Text('22 ํC', style: TextStyle(
      //         fontSize: 11, fontWeight: FontWeight.bold)),
      //   ],
      // ),
      SizedBox(width: 10),
    ],
  );
}