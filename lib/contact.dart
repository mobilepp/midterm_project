import 'package:flutter/material.dart';

class SubjectRegister extends StatelessWidget {
  const SubjectRegister({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBodyContactWidget(),
    );
  }
}

Widget buildBodyContactWidget(){
  return ListView(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("ติดต่อเรา",textScaleFactor: 2.5,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
      ),
      // ListTile(
      //   leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
      //   title: const Text(
      //     'ติดต่อเรา',
      //     style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
      //   ),
      //   // subtitle: Text(
      //   //   'Secondary Text',
      //   //   style: TextStyle(color: Colors.black.withOpacity(0.6)),
      //   // ),
      // ),
      // Text('ติดต่อเรา //ถามในระบบ ช่องทางอื่น'),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          elevation: 10,
          color: Colors.blue.shade50,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          child: Column(
            children: [
              ListTile(
                leading: Icon(Icons.computer_outlined, color: Colors.brown[700],),
                title: const Text(
                  'IT-Clinic',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(
                width: 300,
                height: 90,
                child: Center(
                  child: Text('อาคารสำนักคอมพิวเตอร์ ชั้น 1 ห้อง 102\nจันทร์ - ศุกร์ 08:30 - 16:30 น.\n038-102770\n', textScaleFactor: 1.2,),
                ),
              ),
            ],
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          elevation: 10,
          color: Colors.orange.shade50,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          child: Column(
            children: [
              ListTile(
                leading: Icon(Icons.telegram_outlined, color: Colors.brown[700],),
                title: const Text(
                  'Contact',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(
                width: 350,
                height: 1000,
                child: Center(
                  child: Text('กองทะเบียนและประมวลผลการศึกษา\nมหาวิทยาลัยบูรพา'
                      '\n169 ถ.ลงหาดบางแสน ต.แสนสุข อ.เมือง จ.ชลบุรี 20131\n'
                      'เปิดภาคเรียน : --------------\n'
                      '08:30 - 19:30 น. (จันทร์ พุธ ศุกร์)\n'
                      '08:30 - 16:30 น. (อังคาร พฤหัสบดี เสาร์ อาทิตย์)\n'
                      'หมายเหตุ: ปิดวันหยุดราชการ\n'
                      'ปิดภาคเรียน : --------------\n'
                      '08:30 - 16:30 น. (จันทร์ - ศุกร์)\n'
                      'ปิดเสาร์ - อาทิตย์\n'
                      'โทรศัพท์ (เคาน์เตอร์):	0 3810 2725\n'
                      'โทรสาร: 0 3839 0441, 0 3810 2721\n'
                      'โทรศัพท์ (เจ้าหน้าที่) : --------------\n'
                      'ฝ่ายรับเข้าศึกษาระดับปริญญาตรี: 038-102721 หรือ 038-102643\n'
                      'การเทียบโอนผลการเรียน ตรวจสอบคุณวุฒิ: 038-102718\n'
                      'ตารางเรียน ตารางสอบ: 038-102715\n'
                      'จำนวนนิสิตและบุคคลภายนอก: 038102719\n'
                      'ระบบบริการการศึกษา: 038-102722\n'
                      'งานธรุการ: 038-102719\n'
                      '--------------\n'
                      'ฝ่ายทะเบียนการศึกษา :\n'
                      'ระดับบัณฑิตศึกษา: 038-102724 หรือ 038-102726\n'
                      'ระดับปริญญาตรี:\n'
                      'วิทยาเขตจันทบุรี: 038-102720\n'
                      'วิทยาเขตสระแก้ว: 038-102729\n'
                      'คณะศึกษาศาสตร์: 038-102720\n'
                      'คณะรัฐศาสตร์และนิติศาสตร: 038-102727\n'
                      'คณะการจัดการและการท่องเที่ยว: 038-102729\n'
                      'คณะมนุษยศาสตร์และสังคมศาสตร์: 038-102723\n'
                      'คณะวิศวกรรมศาสตร์: 038-102723\n'
                      'คณะศิลปกรรมศาสตร์: 038-102723\n'
                      'คณะดนตรีและการแสดง: 038-102723\n'
                      'คณะพยาบาลศาสตร์: 038-102726\n'
                      'คณะโลจิสติกส์: 038-102726\n'
                      'คณะการแพทย์แผนไทยอภัยภูเบศร์: 038-102726\n'
                      'คณะวิทยาศาสตร์การกีฬา: 038-102726\n'
                      'คณะแพทย์ศาสตร์: 038-102726\n'
                      'คณะสหเวชศาสตร์: 038-102729\n'
                      'คณะวิทยาศาสตร์: 038-102729\n'
                      'คณะสาธารณสุขศาสตร์: 038-102729\n'
                      'คณะวิทยาการสารสนเทศ: 038-102724\n'
                      'วิทยาลัยนานาชาติ: 038-102724\n'
                      'คณะเภสัชศาสตร์: 038-102724\n'
                      'คณะภูมิสารสนเทศศาสตร์: 038-102724\n'
                      ,textScaleFactor: 1.1,),
                ),
              ),
            ],
          ),
        ),
      ),

      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          elevation: 10,
          color: Colors.lightGreen.shade50,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          child: Column(
            children: [
              ListTile(
                leading: Icon(Icons.phone, color: Colors.brown[700],),
                title: const Text(
                  'เบอร์ติดต่ออื่นๆ',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(
                width: 350,
                height: 250,
                child: Center(
                  child: Text('สายด่วนเทศบาลเมืองแสนสุข: 1132, 081-8637000\n'
                      'สภ.แสนสุข: 081-8637000\n'
                      'งานป้องกันและบรรเทาสาธารณภัยเมืองแสนสุข: 038-381061 หรือ 199\n'
                      'ศูนย์บริการสาธารณสุข (ม.บูรพา): 038-371116\n'
                      'โรงพยาบาลมหาวิทยาลัยบูรพา: 038-390324 (ฉุกเฉิน 1669)\n'
                      'มหาวิทยาลัยบูรพา: 038-102222\n'
                      'กองกิจการนิสิต: 038-102222 ต่อ 2530, 2537\n'
                    ,textScaleFactor: 1.1,),
                ),
              ),
            ],
          ),
        ),
      ),
    ]
  );
}