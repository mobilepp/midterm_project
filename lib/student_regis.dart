import 'package:flutter/material.dart';
import 'package:midterm_project/reg_fpage.dart';
import 'package:midterm_project/reg_mainpage.dart';

class StudentRegister extends StatefulWidget {
  const StudentRegister({super.key});

  @override
  State<StudentRegister> createState() => _StudentRegisterState();
}

class _StudentRegisterState extends State<StudentRegister> {
  Widget select = buildRegisterWidget();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Container(
          padding: EdgeInsets.all(6),
          child:ListView(
            children: [
              buildDtaStdWidget(),
              Divider(
                color: Colors.brown[700],
              ),
              buildSelectYearWidget(),
              buildSelectDurationWidget(),
              Divider(
                color: Colors.brown[700],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Row(
                        children: [
                          IconButton(
                            onPressed: (){
                              setState(() {
                                select = buildRegisterWidget();
                              });
                            },
                            icon: Icon(Icons.edit_note),
                          ),
                          Text('ลงทะเบียน'),
                        ],
                      )
                      ,),
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Row(
                        children: [
                          IconButton(
                            onPressed: (){
                              setState(() {
                                select = buildRegisterReportWidget();
                              });
                            },
                            icon: Icon(Icons.table_view_outlined),
                          ),
                          Text('ผลการลงทะเบียน'),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Row(
                        children: [
                          IconButton(
                            onPressed: (){
                              setState(() {
                                select = buildIncDecWidget();
                              });
                            },
                            icon: Icon(Icons.edit),
                          ),
                          Text('อนุมัติเพิ่ม/ลด'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Divider(
                color: Colors.brown[700],
              ),
              select,
            ],
          )
      )
    );
  }
}

AppBar buildAppBar(BuildContext context){
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.brown[700],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined, //login_outlined, //arrow_back_ios_new_outlined,
          color: Colors.white,
        )
    ),
    title: Text("Student Register"),
    actions: <Widget>[
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.g_translate_outlined,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const RegF()),
            );
          },
          icon: Icon(
            Icons.power_settings_new, //logout_outlined,
            color: Colors.white,
          )
      ),
    ],
  );
}

Widget buildSelectYearWidget(){
  return Row(
    children: [
      Text('ปีการศึกษา:'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.arrow_left_rounded,
          )
      ),
      Text('2565', style: TextStyle( color: Colors.blueGrey[600], fontWeight: FontWeight.bold),),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.arrow_right_rounded,
          )
      ),
    ],
  );
}

Widget buildSelectDurationWidget(){
  return Row(
    children: [
      Icon(Icons.more_vert),
      Text('ภาคเรียน:'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.looks_one_outlined,
          )
      ),
      Text('/'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.looks_two_outlined,
            color: Colors.blueGrey,
          )
      ),
      Text('/'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.sunny,
          )
      ),
      Text('ภาคฤดูร้อน'),
    ],
  );
}

buildRegisterWidget(){
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children:<Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("หน้าลงทะเบียน",textScaleFactor: 1.5,style: TextStyle(fontWeight:FontWeight.bold),),
        ),
        Text('\nนิสิตทำการลงทะเบียนแล้วโปรดตรวจสอบรายวิชาที่เมนูผลการลงทะเบียน', style: TextStyle(fontWeight: FontWeight.bold),),
    ]
  );
}

buildRegisterReportWidget(){
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children:<Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ผลการลงทะเบียน",textScaleFactor: 1.5,style: TextStyle(fontWeight:FontWeight.bold),),
        ),
        Table(
          columnWidths: const <int, TableColumnWidth>{
            0 : FlexColumnWidth(1.5),
            1 : FlexColumnWidth(3),
            2 : FlexColumnWidth(1),
            // 2 : FixedColumnWidth(100),
            3: FlexColumnWidth(1),
            4: FlexColumnWidth(1),
            5: FlexColumnWidth(1),
          },
          defaultColumnWidth: const FlexColumnWidth(),
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: <TableRow>[
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                RegisSubjCell(title: 'รหัสวิชา', isHeader: true),
                RegisSubjCell(title: 'ชื่อรายวิชา', isHeader: true),
                RegisSubjCell(title: 'แบบการศึกษา', isHeader: true),
                RegisSubjCell(title: 'หน่วยกิต', isHeader: true),
                RegisSubjCell(title: 'กลุ่ม', isHeader: true),
                RegisSubjCell(title: 'เกรด', isHeader: true),
              ],
            ),
            ...RegisSubject.getRegisSubjects().map((regisSubj) {
              return TableRow(children: [
                RegisSubjCell(title: regisSubj.subjId),
                RegisSubjCell(title: regisSubj.subjName, isCenData : false),
                RegisSubjCell(title: regisSubj.typeGrade),
                RegisSubjCell(title: '${regisSubj.subjUnit}'),
                RegisSubjCell(title: '${regisSubj.gruop}'),
                RegisSubjCell(title: regisSubj.grade),
              ]);
            }),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: 'จำนวนหน่วยกิตรวม', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '18', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
              ],
            ),
          ],
        ),
      ]
  );
}

buildIncDecWidget(){
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children:<Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ผลการอนุมัติเพิ่ม/ลด",textScaleFactor: 1.5,style: TextStyle(fontWeight:FontWeight.bold),),
        ),
        Container(
          height: 50,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: [
            Table(
            defaultColumnWidth: FixedColumnWidth(70.0),
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            children: <TableRow>[
              TableRow(
                decoration: BoxDecoration(
                  color: Colors.brown[50],
                ),
                children: const <Widget>[
                  RegisSubjCell(title: 'วันที่', isHeader: true),
                  RegisSubjCell(title: 'รหัสวิชา', isHeader: true),
                  RegisSubjCell(title: 'ชื่อรายวิชา', isHeader: true),
                  RegisSubjCell(title: 'หน่วยกิต', isHeader: true),
                  RegisSubjCell(title: 'กลุ่ม', isHeader: true),
                  RegisSubjCell(title: 'แบบการศึกษา', isHeader: true),
                  RegisSubjCell(title: 'อาจารย์\nที่ปรึกษา', isHeader: true),
                  RegisSubjCell(title: 'อาจารย์/\nผู้สอน', isHeader: true),
                  RegisSubjCell(title: 'คณบดี', isHeader: true),
                  RegisSubjCell(title: 'ผลการ\nอนุมัติ', isHeader: true),
                  RegisSubjCell(title: 'หมายเหตุ', isHeader: true),
                ],
              ),
            ],
          ),
              // Table(
              //   defaultColumnWidth: FixedColumnWidth(70.0),
              //   defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              //   border: TableBorder.all(
              //       color: Colors.brown.shade200,
              //       style: BorderStyle.solid,
              //       width: 2),
              //   // columnWidths: {
              //   //   0: FlexColumnWidth(1),
              //   //   1: FlexColumnWidth(2),
              //   //   2: FlexColumnWidth(3),
              //   //   3: FlexColumnWidth(3),
              //   // },
              //   children: [
              //     TableRow(
              //         children: [
              //           Column(children:[Text("วันที่",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
              //           Column(children:[Text("รหัสวิชา",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
              //           Column(children:[Text("ชื่อรายวิชา",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
              //           Column(children:[Text("หน่วยกิต",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
              //           Column(children:[Text("กลุ่ม",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
              //           Column(children:[Text("แบบ",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
              //           Column(children:[Text("อาจารย์\nที่ปรึกษา",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
              //           Column(children:[Text("อาจารย์/\nผู้สอน",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
              //           Column(children:[Text("คณบดี",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
              //           Column(children:[Text("ผลการ\nอนุมัติ",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
              //           Column(children:[Text("หมายเหตุ",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
              //         ]
              //     ),
              //   ],
              // ),
            ],
          ),
        ),
        Text('\nนิสิตจะสามารถเพิ่ม-ลดรายวิชาได้อีกครั้ง เมื่อผลการอนุมัติครบทุกรายการ และยังอยู่ในช่วงเพิ่ม-ลด', style: TextStyle(fontWeight: FontWeight.bold),),
      ]
  );
}

class RegisSubjCell extends StatelessWidget {
  final String title;
  final bool isHeader;
  final bool isCenData;

  const RegisSubjCell({
    Key? key,
    required this.title,
    this.isHeader = false,
    this.isCenData = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isHeader || isCenData ? Alignment.center : Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
            style: TextStyle(
                fontWeight: isHeader ? FontWeight.bold : FontWeight.normal,),
        ),
      ),
    );
  }
}

class RegisSubject {
  final String subjId;
  final String subjName;
  final String typeGrade; //GD = A-F
  final int subjUnit;
  final int gruop;
  final String grade;

  RegisSubject(this.subjId, this.subjName, this.typeGrade, this.subjUnit, this.gruop, this.grade);

  static List<RegisSubject> getRegisSubjects() {
    return [
      RegisSubject('88624359', 'Web Programming \nการเขียนโปรแกรมบนเว็บ', 'GD', 3, 2, ''),
      RegisSubject('88624459', 'Object-Oriented Analysis and Design \nการวิเคราะห์และออกแบบเชิงวัตถุ', 'GD', 3, 2, ''),
      RegisSubject('88624559', 'Software Testing \nการทดสอบซอฟต์แวร์', 'GD', 3, 2, ''),
      RegisSubject('88634259', 'Multimedia Programming for Multiplatforms \nการโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม', 'GD', 3, 2, ''),
      RegisSubject('88634459', 'Mobile Application Development I \nการพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1', 'GD', 3, 2, ''),
      RegisSubject('88646259', 'Introduction to Natural Language Processing \nการประมวลผลภาษาธรรมชาติเบื้องต้น', 'GD', 3, 2, ''),
    ];
  }
}

