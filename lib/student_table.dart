import 'package:flutter/material.dart';
import 'package:midterm_project/reg_mainpage.dart';
import 'package:midterm_project/reg_fpage.dart';

class StudentTable extends StatefulWidget {
  const StudentTable({super.key});

  @override
  State<StudentTable> createState() => _StudentTableState();
}

class _StudentTableState extends State<StudentTable> {
  Widget select = buildScheduleWidget();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Container(
        padding: EdgeInsets.all(6),
        child:ListView(
          children: [
            buildDtaStdWidget(),
            Divider(
              color: Colors.brown[700],
            ),
            buildSelectYearWidget(),
            buildSelectDurationWidget(),
            Divider(
              color: Colors.brown[700],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Align(
                  alignment: Alignment.centerRight,
                    child: Row(
                        children: [
                          IconButton(
                            onPressed: (){
                              setState(() {
                                select = buildScheduleWidget();
                              });
                            },
                            icon: Icon(Icons.table_view_outlined),
                          ),
                        Text('ตารางเรียน'),
                        ],
                      )
                    ,),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: (){
                            setState(() {
                              select = buildMidtermWidget();
                            });
                          },
                          icon: Icon(Icons.table_view_outlined),
                        ),
                        Text('สอบกลางภาค'),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: (){
                            setState(() {
                              select = buildFinalWidget();
                            });
                          },
                          icon: Icon(Icons.table_view_outlined),
                        ),
                        Text('สอบปลายภาค'),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Divider(
              color: Colors.brown[700],
            ),
            select,
          ],
        )
      )
    );
  }
}

AppBar buildAppBar(BuildContext context){
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.brown[700],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined,
          color: Colors.white,
        )
    ),
    title: Text("Student Table"),
    actions: <Widget>[
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.print,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.g_translate_outlined,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const RegF()),
            );
          },
          icon: Icon(
            Icons.power_settings_new,
            color: Colors.white,
          )
      ),
    ],
  );
}

// Widget buildBodyStdTable(){
//   return
//     // Container(
//     //   padding: EdgeInsets.all(6),
//     //   child:
//       ListView(
//         children: [
//           buildDtaStdWidget(),
//           Divider(
//             color: Colors.brown[700],
//           ),
//           buildSelectYearWidget(),
//           buildSelectDurationWidget(),
//         ],
//       // ),
//   );
// }

Widget buildSelectYearWidget(){
  return Row(
    children: [
      Text('ปีการศึกษา:'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.arrow_left_rounded,
          )
        ),
      Text('2565', style: TextStyle( color: Colors.blueGrey[600], fontWeight: FontWeight.bold),),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.arrow_right_rounded,
          )
      ),
    ],
  );
}

Widget buildSelectDurationWidget(){
  return Row(
    children: [
      Icon(Icons.more_vert),
      Text('ภาคเรียน:'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.looks_one_outlined,
          )
      ),
      Text('/'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.looks_two_outlined,
            color: Colors.blueGrey,
          )
      ),
      Text('/'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.sunny,
          )
      ),
      Text('ภาคฤดูร้อน'),
    ],
  );
}

Widget buildScheduleWidget(){
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children:<Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ตารางเรียนรายวิชาที่ลงทะเบียนไว้แล้ว",textScaleFactor: 1.5,style: TextStyle(fontWeight:FontWeight.bold),),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
              elevation: 10,
              color: Colors.amber.shade100,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50),
              ),
              child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                  title: const Text(
                    'วันจันทร์',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 300,
                  child: Center(child: Text('10:00-12:00\n'
                      '88624559-59 Software Testing การทดสอบซอฟต์แวร์\n'
                      '(3) 2, IF-4M210 [IF]\n'
                      '\n13:00-15:00\n'
                      '88624459-59 Object-Oriented Analysis and Design การวิเคราะห์และออกแบบเชิงวัตถุ\n'
                      '(3) 2, IF-3M210 [IF]\n'
                      '\n17:00-19:00\n'
                      '88624359-59 Web Programming การเขียนโปรแกรมบนเว็บ\n'
                      '(3) 2, IF-3M210 [IF]')),
                  ),
                ],
              ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.pink.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                  title: const Text(
                    'วันอังคาร',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 300,
                  child: Center(child: Text('10:00-12:00\n'
                      '88634259-59 Multimedia Programming for Multiplatforms การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม\n'
                      '(3) 2, IF-4C02 [IF]\n'
                      '\n13:00-15:00\n'
                      '88624559-59 Software Testing การทดสอบซอฟต์แวร์\n'
                      '(3) 2, IF-3C03 [IF]\n'
                      '\n17:00-19:00\n'
                      '88624459-59 Object-Oriented Analysis and Design การวิเคราะห์และออกแบบเชิงวัตถุ\n'
                      '(3) 2, IF-3C01 [IF]\n'
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.green.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                  title: const Text(
                    'วันพุธ',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 300,
                  child: Center(child: Text('10:00-12:00\n'
                      '88634459-59 Mobile Application Development I การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1\n'
                      '(3) 2, IF-4C02 [IF]\n'
                      '\n13:00-15:00\n'
                      '88634259-59 Multimedia Programming for Multiplatforms การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม\n'
                      '(3) 2, IF-4C01 [IF]\n'
                      '\n15:00-17:00\n'
                      '88624359-59 Web Programming การเขียนโปรแกรมบนเว็บ\n'
                      '(3) 2, IF-3C01 [IF]')),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.blue.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                  title: const Text(
                    'วันศุกร์',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 200,
                  child: Center(child: Text('09:00-12:00\n'
                      '88646259-59 Introduction to Natural Language Processing การประมวลผลภาษาธรรมชาติเบื้องต้น\n'
                      '(3) 2, IF-5T05 [IF]\n'
                      '\n13:00-15:00\n'
                      '88634459-59 Mobile Application Development I การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1\n'
                      '(3) 2, IF-4C02 [IF]\n'
                  ),
                 ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('* ข้อมูลที่ปรากฎอยู่ในตารางเรียนประกอบด้วย รหัสวิชา (จำนวนหน่วยกิต) กลุ่ม, ห้องเรียนและอาคาร ตามลำดับ',textScaleFactor: 0.9),
        ),
      ]
  );
}

Widget buildMidtermWidget(){
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children:<Widget>[
        Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("ตารางสอบกลางภาครายวิชาที่ลงทะเบียนไว้แล้ว",textScaleFactor: 1.5,style: TextStyle(fontWeight:FontWeight.bold),),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.amber.shade100,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
              ListTile(
                leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                title: const Text(
                'วันจันทร์',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(
                width: 300,
                height: 200,
                child: Center(child: Text('16 ม.ค. 2566 - 09:00-12:00\n'
                '88624559-59 Software Testing การทดสอบซอฟต์แวร์\n'
                '(C) IF-3M210 [IF]\n'
                '\n16 ม.ค. 2566 - 17:00-20:00\n'
                '88624359-59 Web Programming การเขียนโปรแกรมบนเว็บ\n'
                '(L) IF-4C02 [IF]')),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.pink.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                  title: const Text(
                  'วันอังคาร',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 100,
                  child: Center(child: Text('17 ม.ค. 2566 - 17:00-20:00\n'
                  '88624459-59 Object-Oriented Analysis and Design การวิเคราะห์และออกแบบเชิงวัตถุ\n'
                  '(C) IF-11M280 [IF]\n'
                  ),
                ),
              ),
             ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.green.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                  title: const Text(
                  'วันพุธ',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 200,
                  child: Center(child: Text('18 ม.ค. 2566 - 13:00-16:00\n'
                      '88634259-59 Multimedia Programming for Multiplatforms การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม\n'
                      '(L) IF-7T01 [IF]\n'
                      '\n18 ม.ค. 2566 - 17:00-20:00\n'
                      '88646259-59 Introduction to Natural Language Processing การประมวลผลภาษาธรรมชาติเบื้องต้น\n'
                      '(C) IF-11M280 [IF]\n')),
                  ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.blue.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                  title: const Text(
                  'วันศุกร์',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 100,
                  child: Center(child: Text('20 ม.ค. 2566 - 13:00-16:00\n'
                    '88634459-59 Mobile Application Development I การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1\n'
                    '(L) ARR-สอบออนไลน์\n'
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('หมายเหตุ   C = Lecture  L = Lab  R = ประชุม  S = Self Study  T = ติว',textScaleFactor: 0.9),
        ),
      ]
  );
}

Widget buildFinalWidget(){
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children:<Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ตารางสอบปลายภาครายวิชาที่ลงทะเบียนไว้แล้ว",textScaleFactor: 1.5,style: TextStyle(fontWeight:FontWeight.bold),),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.amber.shade100,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                  title: const Text(
                    'วันจันทร์',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 100,
                  child: Center(child: Text('-')),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.pink.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                  title: const Text(
                    'วันอังคาร',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 100,
                  child: Center(child: Text('-'
                  ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.green.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                  title: const Text(
                    'วันพุธ',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 100,
                  child: Center(child: Text('-')),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.blue.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
                  title: const Text(
                    'วันศุกร์',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 100,
                  child: Center(child: Text('-'),),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('หมายเหตุ   C = Lecture  L = Lab  R = ประชุม  S = Self Study  T = ติว',textScaleFactor: 0.9),
        ),
      ]
  );
}


// Container(
//   padding: const EdgeInsets.all(16.0),
//   height: 200,
//   child: ListView(
//     scrollDirection: Axis.horizontal,
//     children: <Widget>[
// Table(
//   defaultColumnWidth: FixedColumnWidth(80.0),
//   defaultVerticalAlignment: TableCellVerticalAlignment.middle,
//   border: TableBorder.all(
//       color: Colors.brown.shade200,
//       style: BorderStyle.solid,
//       width: 2),
//   // columnWidths: {
//   //   0: FlexColumnWidth(1),
//   //   1: FlexColumnWidth(2),
//   //   2: FlexColumnWidth(3),
//   //   3: FlexColumnWidth(3),
//   // },
//   children: [
//     TableRow(
//         children: [
//         Column(children:[Text("\nDay/Time\n",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//         Column(children:[Text("09:00-10:00",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//         Column(children:[Text("10:00-11:00",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//         Column(children:[Text("11:00-12:00",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//         Column(children:[Text("12:00-13:00",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//         Column(children:[Text("13:00-14:00",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//         Column(children:[Text("14:00-15:00",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//         Column(children:[Text("15:00-16:00",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//         Column(children:[Text("16:00-17:00",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//         Column(children:[Text("17:00-18:00",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//         Column(children:[Text("18:00-19:00",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//       ]
//     ),
//     TableRow(
//         children: [
//           Column(children:[Text("จันทร์",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("10:00-11:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("11:00-12:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("13:00-14:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("14:00-15:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("17:00-18:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("18:00-19:00",textScaleFactor: 0.9)]),
//         ]
//     ),
//     TableRow(
//         children: [
//           Column(children:[Text("อังคาร",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("10:00-11:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("11:00-12:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("13:00-14:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("14:00-15:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("17:00-18:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("18:00-19:00",textScaleFactor: 0.9)]),
//         ]
//     ),
//     TableRow(
//         children: [
//           Column(children:[Text("พุธ",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("10:00-11:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("11:00-12:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("13:00-14:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("14:00-15:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("15:00-16:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("16:00-17:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//         ]
//     ),
//     TableRow(
//         children: [
//           Column(children:[Text("พฤหัสบดี",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//         ]
//     ),
//     TableRow(
//         children: [
//           Column(children:[Text("ศุกร์",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
//           Column(children:[Text("09:00-10:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("10:00-11:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("11:00-12:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("13:00-14:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("14:00-15:00",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//           Column(children:[Text("",textScaleFactor: 0.9)]),
//         ]
//     ),
//   ],
// ),
//     ],
//   ),
// ),