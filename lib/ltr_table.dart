import 'package:flutter/material.dart';

class SubjectRegister extends StatelessWidget {
  const SubjectRegister({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBodyTableLTRWidget(),
    );
  }
}

Widget buildBodyTableLTRWidget(){
  return ListView(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("ตารางเรียน/สอน/ใช้ห้อง",textScaleFactor: 2.5,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
      ),
      // Text('ตารางเรียน/สอน/ใช้ห้อง'),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: const Steps(),
      ),
    ]
  );
}

class Step {
  Step(
      this.title,
      this.body,
      [this.isExpanded = false]
      );
  String title;
  Widget body;
  bool isExpanded;
}

List<Step> getSteps() {
  return [
    Step('ตารางเรียนนิสิต', getTableStdData()),
    Step('ตารางสอนอาจารย์', getTableTeacherData()),
    Step('ตารางการใช้ห้อง', getTableRoomData()),
  ];
}

Widget getTableStdData(){
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("ค้นหาตารางเรียนนิสิต",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
      ),
      Container(
        padding: const EdgeInsets.all(10),
        child: TextField(
          autofocus: true,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'โปรดระบุเลขประจำตัวนิสิต หรือ ชื่อ-สกุล',
          ),
        ),
      ),
      Container(
          height: 50,
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
          child: ElevatedButton(
            child: const Text('ค้นหา'),
            onPressed: () {},
          )
      ),
      Text('คำแนะนำ\n'
          '1. ถ้าต้องการค้นหานิสิตที่มีชื่อขึ้นต้นด้วย สม ให้ป้อน สม*\n'
          '2. ถ้าต้องการค้นหานิสิตที่มีชื่อลงท้ายด้วย ชาย ให้ป้อน *ชาย\n'
          '3. กดปุ่ม ค้นหา เพื่อเริ่มทำการค้นหาตามเงื่อนไข\n'),
    ],
  );
}

Widget getTableTeacherData(){
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("ค้นหาตารางสอนอาจารย์",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
      ),
      Container(
        padding: const EdgeInsets.all(10),
        child: TextField(
          autofocus: true,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'โปรดระบุชื่อท่านอาจารย์',
          ),
        ),
      ),
      Container(
          height: 50,
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
          child: ElevatedButton(
            child: const Text('ค้นหา'),
            onPressed: () {},
          )
      ),
      Text('คำแนะนำ\n'
          '1. ถ้าต้องการค้นหาอาจารย์ที่มีชื่อขึ้นต้นด้วย สม ให้ป้อน สม*\n'
          '2. ถ้าต้องการค้นหาอาจารย์ที่มีชื่อลงท้ายด้วย ชาย ให้ป้อน *ชาย\n'
          '3. กดปุ่ม ค้นหา เพื่อเริ่มทำการค้นหาตามเงื่อนไข\n'),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.grid_view_sharp, color: Colors.blue[200],),
          Text('กลุ่มรียนปกติ'),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.grid_view_sharp, color: Colors.red[200],),
          Text('ห้องเรียนซ้ำซ้อน'),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.grid_view_sharp, color: Colors.green[200],),
          Text('มีหลายกลุ่มเรียน'),
        ],
      ),
      Text('')
    ],
  );
}

Widget getTableRoomData(){
  return Column(
      children: [
        // Padding(
        //   padding: const EdgeInsets.all(8.0),
        //   child: Text("ตารางการใช้ห้อง",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
        // ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.mouse,),
            Text('ใช้เม้าส์คลิ้กที่รหัสอาคารเพื่อเลือกห้อง', style: TextStyle(fontWeight:FontWeight.bold)),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("วิทยาเขต บางแสน",textScaleFactor: 1.1,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              border: TableBorder.all(
                  color: Colors.brown.shade200,
                  style: BorderStyle.solid,
                  width: 2),
              columnWidths: {
                0: FlexColumnWidth(2),
                1: FlexColumnWidth(4),
              },
              children: [
                TableRow(
                    children: [
                      Column(crossAxisAlignment: CrossAxisAlignment.center,
                          children:[
                            Row(children: [
                              Padding(padding: EdgeInsets.all(6.0), child: Text("0",textScaleFactor: 1.1,style: TextStyle(fontWeight:FontWeight.bold),),),
                              Icon(Icons.ads_click)],)
                          ]),
                      Column(children:[Text("-ไม่กำหนด-",textScaleFactor: 0.9)]),
                    ]
                ),
                TableRow(
                    children: [
                      Column(crossAxisAlignment: CrossAxisAlignment.center,
                          children:[
                            Row(children: [
                              Padding(padding: EdgeInsets.all(6.0), child: Text("AB",textScaleFactor: 1.1,style: TextStyle(fontWeight:FontWeight.bold),),),
                              Icon(Icons.ads_click)],)
                          ]),
                      Column(children:[Text("อาคารศิลปกรรมศาสตร์",textScaleFactor: 0.9)]),
                    ]
                ),
                TableRow(
                    children: [
                      Column(crossAxisAlignment: CrossAxisAlignment.center,
                          children:[
                            Row(children: [
                              Padding(padding: EdgeInsets.all(6.0), child: Text("AC",textScaleFactor: 1.1,style: TextStyle(fontWeight:FontWeight.bold),),),
                              Icon(Icons.ads_click)],)
                          ]),
                      Column(children:[Text("หอศิลปะและวัฒนธรรมภาคตะวันออก",textScaleFactor: 0.9)]),
                    ]
                ),
                TableRow(
                    children: [
                      Column(crossAxisAlignment: CrossAxisAlignment.center,
                          children:[
                            Row(children: [
                              Padding(padding: EdgeInsets.all(6.0), child: Text("..",textScaleFactor: 1.1,style: TextStyle(fontWeight:FontWeight.bold),),),
                              Icon(Icons.ads_click)],)
                          ]),
                      Column(children:[Text("..",textScaleFactor: 0.9)]),
                    ]
                ),
              ]
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("วิทยาเขต จันทบุรี\n...",textScaleFactor: 1.1,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("วิทยาเขต สระแก้ว\n...",textScaleFactor: 1.1,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
        ),
      ]
  );
}


class Steps extends StatefulWidget {
  const Steps({Key? key}) : super(key: key);
  @override
  State<Steps> createState() => _StepsState();
}

class _StepsState extends State<Steps> {
  final List<Step> _steps = getSteps();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: _renderSteps(),
      ),
    );
  }
  Widget _renderSteps() {
    return ExpansionPanelList(
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          _steps[index].isExpanded = !isExpanded;
        });
      },
      children: _steps.map<ExpansionPanel>((Step step) {
        return ExpansionPanel(
          backgroundColor: step.isExpanded ? Colors.white : Colors.brown.shade50,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return ListTile(
              title: Text(step.title, style: TextStyle(fontWeight: FontWeight.bold,),textScaleFactor: 1.2,),
            );
          },
          body: step.body,
          isExpanded: step.isExpanded,
        );
      }).toList(),
    );
  }
}