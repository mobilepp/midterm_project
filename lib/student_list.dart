import 'package:flutter/material.dart';
import 'package:midterm_project/reg_fpage.dart';

class StudentList extends StatefulWidget {
  const StudentList({super.key});

  @override
  State<StudentList> createState() => _StudentListState();
}

class _StudentListState extends State<StudentList> {
  final List<Map> stdList = [
    { "id" : "1", "stdid" : "63160002", "name" : "นายก้องภพ เคนรัง", "status" : "10" },
    { "id" : "2", "stdid" : "63160003", "name" : "นายจิรัฎฐ์กฤศ ผ่องวรรณ์", "status" : "50" },
    { "id" : "3", "stdid" : "63160004", "name" : "นายณัฏฐชัย จันทร์ศรี", "status" : "10" },
    { "id" : "4", "stdid" : "63160005", "name" : "นายทักษ์ติโชค อนุมอญ", "status" : "10" },
    { "id" : "5", "stdid" : "63160006", "name" : "นายธันวา แสนสุด", "status" : "10" },
    { "id" : "6", "stdid" : "63160007", "name" : "นางสาวนนณี รอดมา", "status" : "10" },
    { "id" : "7", "stdid" : "63160008", "name" : "นางสาวนนทลี สามารถ", "status" : "10" },
    { "id" : "8", "stdid" : "63160009", "name" : "นายภาดล วงษ์ศิริ", "status" : "10" },
    { "id" : "9", "stdid" : "63160010", "name" : "นางสาวมนัสชนก แซ่เตียว", "status" : "10" },
    { "id" : "10", "stdid" : "63160011", "name" : "นายวริทธิ์ภัทร คีรีรัตน์", "status" : "10" },
    { "id" : "..", "stdid" : "..", "name" : "..", "status" : ".." },

  ];

  // List id = ["1","2"];
  // List stdid = ["63160002","63160003"];
  // List name = ["นายก้องภพ เคนรัง","นายจิรัฎฐ์กฤศ ผ่องวรรณ์"];
  // List status = ["10","50"];
  //
  // final List<Map> listOfColumns = [
  //   {"Name": "AAAAAA", "Number": "1", "State": "Yes"},
  //   {"Name": "BBBBBB", "Number": "2", "State": "no"},
  //   {"Name": "CCCCCC", "Number": "3", "State": "Yes"}
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBodyStdListWidget(stdList),
    );
  }
}

AppBar buildAppBar(BuildContext context){
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.brown[700],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined, //login_outlined, //arrow_back_ios_new_outlined,
          color: Colors.white,
        )
    ),
    title: Text("Student List"),
    actions: <Widget>[
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.print,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.g_translate_outlined,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const RegF()),
            );
          },
          icon: Icon(
            Icons.power_settings_new, //logout_outlined,
            color: Colors.white,
          )
      ),
    ],
  );
}

buildBodyStdListWidget(final List<Map> stdList){
  return
    ListView(
          // scrollDirection: Axis.horizontal,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("รายชื่อนิสิต",textScaleFactor: 1.5,style: TextStyle(fontWeight:FontWeight.bold), textAlign: TextAlign.center,),
            ),
            Padding(
            padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 130,
                color: Colors.brown[50],
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("วิทยาเขต : บางแสน\n"
                        "ระดับการศึกษา : ปริญญาตรี ปกติ\n"
                        "คณะ : คณะวิทยาการสารสนเทศ\n"
                        "หลักสูตร 2115020 : วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ\n"
                        "ปีการศึกษาที่เข้า : 2563",style: TextStyle(fontWeight:FontWeight.bold),),
                ),
              ),
            ),
            SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: DataTable(
                columns: [
                  DataColumn(label: Text('ID',style: TextStyle(fontWeight:FontWeight.bold))),
                  DataColumn(label: Text('Std ID',style: TextStyle(fontWeight:FontWeight.bold))),
                  DataColumn(label: Text('Name',style: TextStyle(fontWeight:FontWeight.bold))),
                  DataColumn(label: Text('Status',style: TextStyle(fontWeight:FontWeight.bold))),
                ],
                rows:
                  stdList // Loops through dataColumnText, each iteration assigning the value to element
                    .map(
                  ((element) => DataRow(
                    cells: <DataCell>[
                      DataCell(Text(element["id"])), //Extracting from Map element the value
                      DataCell(Text(element["stdid"])),
                      DataCell(Text(element["name"])),
                      DataCell(Text(element["status"])),
                    ],
                  )),
                )
                .toList(),
              )
            )

    //         ListView.builder(
    //           itemCount:id.length,
    //           prototypeItem: Row(
    //             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //                   children: [
    //                     Column(children:[Text(id[0])]),
    //                     Column(children:[Text(stdid[0])]),
    //                     Column(children:[Text(name[0])]),
    //                     Column(children:[Text(status[0])]),
    //                     ]
    //               ),
    //           itemBuilder: (context, index) {
    //             return Row(
    //                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //                 children: [
    //                   Column(children:[Text(id[index])]),
    //                   Column(children:[Text(stdid[index])]),
    //                   Column(children:[Text(name[index])]),
    //                   Column(children:[Text(status[index])]),
    //                 ]
    //             );
    //           }
          ],

  );
}