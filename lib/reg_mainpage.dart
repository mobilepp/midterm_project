import 'package:flutter/material.dart';
import 'package:side_navigation/side_navigation.dart';
import 'package:midterm_project/my_flutter_app_icons.dart';
import 'package:midterm_project/student_profile.dart';
import 'package:midterm_project/student_table.dart';
import 'package:midterm_project/student_overdue.dart';
import 'package:midterm_project/student_regis.dart';
import 'package:midterm_project/student_grade.dart';
import 'package:midterm_project/student_list.dart';
import 'package:midterm_project/subject_regis.dart';
import 'package:midterm_project/reg_fpage.dart';
import 'package:midterm_project/ltr_table.dart';
import 'package:midterm_project/std_calendar.dart';
import 'package:midterm_project/doc_graduation.dart';
import 'package:midterm_project/doc_rela.dart';
import 'package:midterm_project/contact.dart';

class Reg extends StatefulWidget {
  const Reg({Key? key}) : super(key: key);

  @override
  State<Reg> createState() => _RegState();
}

class _RegState extends State<Reg> {
  List<Widget> views = [];
  int selectedIndex = 0;
  int toggle = 0;
  @override
  Widget build(BuildContext context) {
    setState(() {
      views = [
        Center(
          child: buildBodyMainWidget(context), //Text('หน้าหลัก //ปุ่มไปหน้าต่างๆ ที่เพิ่มเติมเข้ามาหลังล็อกอิน'),
        ),
        Center(
          child: buildBodySubjWidget(), //Text('หลักสูตร/วิชาที่เปิดสอน //แนะนำการลงทะเบียน ค้นหา'),
        ),
        Center(
          child: buildBodyTableLTRWidget(), //Text('ตารางเรียน/สอน/ใช้ห้อง'),
        ),
        Center(
          child: buildBodyStdCarendarWidget(), //Text('ปฏิทินการศึกษา //ดู ดาวน์โหลด'),
        ),
        Center(
          child: buildBodyDocGradeWidget(), //Text('การสำเร็จการศึกษา //ผู้สำเร็จการศึกษา ตรวจสอบวันสำเร็จการศึกษา การยื่นขอสำเร็จการศึกษา'),
        ),
        Center(
          child: buildBodyDocRelaWidget(), //Text('เอกสาร/ข้อมูลต่างๆ ที่เกี่ยวข้อง //แบบฟอร์ม ค่าธรรมเนียม ระเบียบข้อบังคับ สถิตินิสิต ข้อมูลรายงานตัว สารสนเทศเจ้าหน้าที่'),
        ),
        Center(
          child: buildBodyContactWidget(), //Text('ติดต่อเรา //ถามในระบบ ช่องทางอื่น'),
        ),
      ];
    });

    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primarySwatch: Colors.brown,
            textTheme: TextTheme(
              bodyText2: TextStyle(color: Colors.brown[900]),
              subtitle1: TextStyle(color: Colors.brown[900]),
            ),
            iconTheme: IconThemeData(
              color: Colors.brown[900],
            )

        ),
      home: Scaffold(
        appBar: buildAppBar(context),
        body: //buildBodyMainWidget(),
          Row(
            children: [
              SideNavigationBar(
                header: SideNavigationBarHeader(
                    image: CircleAvatar(
                      child: Image(
                        image: AssetImage('images/mukku.jpg'),
                        fit: BoxFit.cover,
                      ),
                      // Icon(Icons.person),
                    ),
                    title: Text('63160066'),
                    subtitle: Text('จุฑามาศ ลืออริยทรัพย์')
                ),
                footer: SideNavigationBarFooter(
                    label: Text('Expanded Now!')
                ),
                selectedIndex: selectedIndex,
                items: const [
                  SideNavigationBarItem(
                    icon: Icons.home,
                    label: 'หน้าหลัก', //ปุ่มไปหน้าต่างๆ ที่เพิ่มเติมเข้ามาหลังล็อกอิน
                  ),
                  SideNavigationBarItem(
                    icon: Icons.subject,
                    label: 'หลักสูตร/วิชาที่เปิดสอน', //แนะนำการลงทะเบียน ค้นหา
                  ),
                  SideNavigationBarItem(
                    icon: Icons.table_view,
                    label: 'ตารางเรียน/สอน/ใช้ห้อง',
                  ),
                  SideNavigationBarItem(
                    icon: Icons.calendar_month,
                    label: 'ปฏิทินการศึกษา', //ดู ดาวน์โหลด
                  ),
                  SideNavigationBarItem(
                    icon: CustomIcons.graduation_cap_1, //Icons.grade,
                    label: 'เกี่ยวกับการสำเร็จการศึกษา', //ผู้สำเร็จการศึกษา ตรวจสอบวันสำเร็จการศึกษา การยื่นขอสำเร็จการศึกษา
                  ),
                  SideNavigationBarItem(
                    icon: CustomIcons.doc, //Icons.document_scanner,
                    label: 'เอกสาร/ข้อมูลต่างๆ ที่เกี่ยวข้อง', //แบบฟอร์ม ค่าธรรมเนียม ระเบียบข้อบังคับ สถิตินิสิต ข้อมูลรายงานตัว สารสนเทศเจ้าหน้าที่
                  ),
                  SideNavigationBarItem(
                    icon: CustomIcons.contacts, //Icons.contact_support,
                    label: 'ติดต่อเรา', //ถามในระบบ ช่องทางอื่น
                  ),
                ],
                onTap: (index) {
                  setState(() {
                    selectedIndex = index;
                  });
                },
                theme: SideNavigationBarTheme(
                  backgroundColor: Colors.brown[50],
                  itemTheme: SideNavigationBarItemTheme(
                      unselectedItemColor: Colors.brown[700],
                      selectedItemColor: Colors.blueGrey,
                      iconSize: 32.5,
                      labelTextStyle: TextStyle(
                          fontSize: 20,
                          color: Colors.black
                      )
                  ),
                  togglerTheme: SideNavigationBarTogglerTheme.standard(),
                  dividerTheme: SideNavigationBarDividerTheme.standard(),
                ),
                toggler: SideBarToggler(
                    expandIcon: Icons.keyboard_arrow_right,
                    shrinkIcon: Icons.keyboard_arrow_left,
                    onToggle: () {
                      print('Toggle $toggle' );
                      setState(() {
                        ++toggle;
                      });
                    }),
              ),
              /// Make it take the rest of the available width
              Flexible(
                // child: toggle % 2 != 0 ? views.elementAt(selectedIndex): Container(),
                child: views.elementAt(selectedIndex),
              )
            ],
          )
      )
    );
  }
}

AppBar buildAppBar(context){
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.brown[700],
    // leading: IconButton(
    //     onPressed: (){},
    //     icon: Icon(
    //       Icons.home_outlined, //login_outlined, //arrow_back_ios_new_outlined,
    //       color: Colors.white,
    //     )
    // ),
    title: Text("Reg BUU App"),
    actions: <Widget>[
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.g_translate_outlined,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const RegF()),
            );
          },
          icon: Icon(
            Icons.power_settings_new, //logout_outlined,
            color: Colors.white,
          )
      ),
    ],
  );
}

Widget buildBodyMainWidget(BuildContext context){
  return ListView (
    children: <Widget>[
      Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                  width: double.infinity,
                  height: 170, //MediaQuery.of(context).size.height * 0.2,
                child: Image.network(
                    "https://images.rawpixel.com/image_png_600/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvdjEwMjAtYmctMDFiLnBuZw.png",
                    // "https://images.rawpixel.com/image_500/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvcm00NTItZi0wMTQuanBn.jpg",
                    // "https://images.rawpixel.com/image_600/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvcGYtczEwMC1hMDIuanBn.jpg",
                    //"https://t4.ftcdn.net/jpg/04/61/23/23/360_F_461232389_XCYvca9n9P437nm3FrCsEIapG4SrhufP.jpg",
                    fit: BoxFit.cover,
                  )
              ),
              Positioned(
                top: 25,
                left: 70,
                child: Column(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.brown.withOpacity(0.5),
                          spreadRadius: 3.0,
                          blurRadius: 7.0,
                          offset: Offset(3.0, 3.0),
                          )
                        ],
                      ),
                      child: Image(
                        image: AssetImage('images/buu_logo.png'),
                        height: 50, width: 50,
                      ),
                      // Image.network(
                      //   "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Buu-logo11.png/903px-Buu-logo11.png?20221010124315",
                      //   height: 50, width: 50,
                      // ),
                ),
                    Text('ระบบบริการการศึกษา' '\nมหาวิทยาลัยบูรพา',
                      style: TextStyle(
                        fontSize: 23, fontWeight: FontWeight.bold, color: Colors.brown[900],
                        shadows: [
                          Shadow(
                            blurRadius: 5.0,
                            color: Colors.brown.shade300,
                            offset: Offset(5.0, 5.0),
                          ),
                        ],
                      ), textAlign: TextAlign.center,),
                  ]
                  ),
                ),

                // ListTile(
                //   leading:
                //   Image.network(
                //     "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Buu-logo11.png/903px-Buu-logo11.png?20221010124315",
                //     fit: BoxFit.cover, height: 50, width: 50,
                //   ),
                //   // Image(
                //   //   image: AssetImage('images/buu_logo.png'),
                //   //   fit: BoxFit.cover,height: 60,
                //   // ),
                //   title: Text('ระบบบริการการศึกษา' '\nมหาวิทยาลัยบูรพา', style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.brown[900]),),
                // ),

            ],
          ),
          Container(
            height: 50,
            color: Colors.brown[50],
            child: ListTile(
              title: Text('ประกาศ!!!', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.brown[900]),),
              trailing: IconButton(
                  onPressed: (){},
                  icon: Icon(
                    Icons.navigate_next,
                    size: 20,
                    color: Colors.brown[900],
                  )
              ),
            ),
          ),
        ],
      ),
      Divider(
        color: Colors.brown[700],
      ),
      Container(
        height: 150.0,
        child: buildListAdvertise(25, 20),
      ),
      Divider(
        color: Colors.brown[700],
      ),
      buildDtaStdWidget(),
      Divider(
        color: Colors.brown[700],
      ),
      Container(
        height: 100,
        child: buildRegButton(context, 29, 20),
      ),
      Container(
        height: 100,
        child: buildStdButton(context, 28, 20),
      ),
    ],
  );
}

Widget buildDtaStdWidget(){
  return Container(
    height: 130,
    color: Colors.brown[50],
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text('63160066 นางสาวจุฑามาศ ลืออริยทรัพย์ \nคณะวิทยาการสารสนเทศ \nหลักสูตร: 2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 \nป.ตรี 4 ปี ปกติ : วิชาโท: 0: - \nสถานภาพ: กำลังศึกษา \nอ. ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม, ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน',
        style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.brown[900]),),
    ),
  );
}

Widget buildRegButton(BuildContext context, spacew, sizew) {
  return Row( //ListView(
      // scrollDirection: Axis.horizontal,
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
        SizedBox(width: spacew),
        Column(
          children: <Widget>[
          IconButton(
            icon: Icon(
              Icons.app_registration,
              size: sizew+5,
                // color: Colors.indigo.shade800,
            ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const StudentRegister()),
            );
          },
          ),
          Text("ลงทะเบียน", textAlign: TextAlign.center,),
          ],
        ),
        SizedBox(width: spacew),
        Column(
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.table_view_outlined,
                size: sizew+5,
                // color: Colors.indigo.shade800,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const StudentTable()),
                );
              },
            ),
            Text("ตารางเรียน/สอบ", textAlign: TextAlign.center,),
          ],
        ),
        SizedBox(width: spacew),
        Column(
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.grading_outlined,
                size: sizew,
                // color: Colors.indigo.shade800,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const StudentGrade()),
                );
              },
            ),
            Text("ผลการศึกษา/\nตรวจสอบจบ", textAlign: TextAlign.center,),
          ],
        ),
        SizedBox(width: spacew),
      ],
    );
}

Widget buildStdButton(BuildContext context, spacew, sizew){
  return Row( //ListView(
    // scrollDirection: Axis.horizontal,
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          IconButton(
            icon: Icon(
              Icons.person_outline,
              size: sizew+5,
              // color: Colors.indigo.shade800,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const StudentProfile()),
              );
            },
          ),
          Text("ประวัตินิสิต/\nบัญชีผู้ใช้", textAlign: TextAlign.center,),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          IconButton(
            icon: Icon(
              CustomIcons.wallet, //Icons.mail_lock_outlined,
              size: sizew,
              // color: Colors.indigo.shade800,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const StudentOverdue()),
              );
            },
          ),
          Text("ภาระค่าใช้จ่าย", textAlign: TextAlign.center,),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          IconButton(
            icon: Icon(
              Icons.people_alt_outlined,
              size: sizew+5,
              // color: Colors.indigo.shade800,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const StudentList()),
              );
            },
          ),
          Text("รายชื่อนิสิต", textAlign: TextAlign.center,),
        ],
      ),
      SizedBox(width: spacew),
    ],
  );
}

Widget buildListAdvertise(spacew, sizew){
  // double? spacew = 25;
  // double? sizew = 20;
  double width = 190;
  double height = 120;
  return ListView(
    // This next line does the trick.
    scrollDirection: Axis.horizontal,
    children: <Widget>[
      SizedBox(width: 10),
      Column(
        children: <Widget>[
          Container(
            width: width,
            height: height,
            child: Image(
              image: AssetImage('images/pay652.jpg'),
              fit: BoxFit.cover,
            )
          ),
        Flexible(
          child: Container(
            width: width,
            child: Text('กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565', style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ),
          ),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Container(
              width: width,
              height: height,
              child: Image.network("https://images.rawpixel.com/image_600/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvdjkyNi1hZGotMDMxXzJfMS5qcGc.jpg",
                fit: BoxFit.cover,
              )
          ),
          Flexible(
            child: Container(
              width: width,
              child: Text('advertise 2', style: TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
              ),
          ),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Container(
              width: width,
              height: height,
              child: Image.network("https://images.rawpixel.com/image_600/cHJpdmF0ZS9sci9pbWFnZXMvd2Vic2l0ZS8yMDIyLTA1L3BmLXM5Ny1rdC0wMTYzLTAxLmpwZw.jpg",
                fit: BoxFit.cover,
              )
          ),
          Flexible(
            child: Container(
              width: width,
              child: Text('advertise 3', style: TextStyle(
                  fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ),
          ),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Container(
              width: width,
              height: height,
              child: Image.network("https://t4.ftcdn.net/jpg/04/61/23/23/360_F_461232389_XCYvca9n9P437nm3FrCsEIapG4SrhufP.jpg",
                fit: BoxFit.cover,
              )
          ),
          Flexible(
            child: Container(
              width: width,
              child: Text('advertise 4', style: TextStyle(
                  fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ),
          ),
        ],
      ),
      // Column(
      //   children: <Widget>[
      //     Text('01', style: TextStyle(
      //         fontSize: 11, fontWeight: FontWeight.bold)),
      //     Icon(CustomIcons.cloud_moon, color: Colors.indigoAccent, size: sizew,),
      //     Text('22 ํC', style: TextStyle(
      //         fontSize: 11, fontWeight: FontWeight.bold)),
      //   ],
      // ),
      SizedBox(width: 10),
    ],
  );
}