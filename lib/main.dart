import 'package:device_preview/device_preview.dart';
// import 'package:side_navigation/side_navigation.dart';
// import 'package:midterm_project/my_flutter_app_icons.dart';
import 'package:flutter/material.dart';
import 'reg_mainpage.dart';
import 'reg_fpage.dart';

void main() {
  runApp(const RegApp());
}

class RegApp extends StatefulWidget {
  const RegApp({Key? key}) : super(key: key);

  @override
  State<RegApp> createState() => _RegAppState();
}

class _RegAppState extends State<RegApp> {
  // List<Widget> views = [
  //   Center(
  //     child: buildBodyMainWidget(), //Text('หน้าหลัก //ปุ่มไปหน้าต่างๆ ที่เพิ่มเติมเข้ามาหลังล็อกอิน'),
  //   ),
  //   Center(
  //     child: Text('หลักสูตร/วิชาที่เปิดสอน //แนะนำการลงทะเบียน ค้นหา'),
  //   ),
  //   Center(
  //     child: Text('ตารางเรียน/สอน/ใช้ห้อง'),
  //   ),
  //   Center(
  //     child: Text('ปฏิทินการศึกษา //ดู ดาวน์โหลด'),
  //   ),
  //   Center(
  //     child: Text('การสำเร็จการศึกษา //ผู้สำเร็จการศึกษา ตรวจสอบวันสำเร็จการศึกษา การยื่นขอสำเร็จการศึกษา'),
  //   ),
  //   Center(
  //     child: Text('เอกสาร/ข้อมูลต่างๆ ที่เกี่ยวข้อง //แบบฟอร์ม ค่าธรรมเนียม ระเบียบข้อบังคับ สถิตินิสิต ข้อมูลรายงานตัว สารสนเทศเจ้าหน้าที่'),
  //   ),
  //   Center(
  //     child: Text('ติดต่อเรา //ถามในระบบ ช่องทางอื่น'),
  //   ),
  // ];
  // /// The currently selected index of the bar
  // int selectedIndex = 0;
  // int toggle = 0;
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
        tools: const [
          DeviceSection(),
        ],
        builder: (context) => MaterialApp(
          debugShowCheckedModeBanner: false,
          useInheritedMediaQuery: true,
          builder: DevicePreview.appBuilder,
          locale: DevicePreview.locale(context),
          title: 'Responsive and adaptive UI in Flutter',
          theme: ThemeData(
            primarySwatch: Colors.brown,
            textTheme: TextTheme(
              bodyText2: TextStyle(color: Colors.brown[900]),
              subtitle1: TextStyle(color: Colors.brown[900]),
            ),
              iconTheme: IconThemeData(
                color: Colors.brown[900],
              )

          ),
          home:
          // Scaffold(
            // appBar: buildFAppBar(context),//buildAppBar(),
            // body:
            RegF(),//Reg(),
          ),
          // Scaffold(
            // backgroundColor: Colors.white,
            // appBar: buildAppBar(),
            // body: //buildBodyMainWidget(),
              // Row(
              //   // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //   // crossAxisAlignment: CrossAxisAlignment.stretch,
              //   children: [
              //     SideNavigationBar(
              //       header: SideNavigationBarHeader(
              //           image: CircleAvatar(
              //             child: Icon(Icons.person),
              //           ),
              //           title: Text('63160066'),
              //           subtitle: Text('จุฑามาศ ลืออริยทรัพย์')
              //       ),
              //       footer: SideNavigationBarFooter(
              //           label: Text('Expanded Now!')
              //       ),
              //       selectedIndex: selectedIndex,
              //       items: const [
              //         SideNavigationBarItem(
              //           icon: Icons.home,
              //           label: 'หน้าหลัก', //ปุ่มไปหน้าต่างๆ ที่เพิ่มเติมเข้ามาหลังล็อกอิน
              //         ),
              //         SideNavigationBarItem(
              //           icon: Icons.subject,
              //           label: 'หลักสูตร/วิชาที่เปิดสอน', //แนะนำการลงทะเบียน ค้นหา
              //         ),
              //         SideNavigationBarItem(
              //           icon: Icons.table_view,
              //           label: 'ตารางเรียน/สอน/ใช้ห้อง',
              //         ),
              //         SideNavigationBarItem(
              //           icon: Icons.calendar_month,
              //           label: 'ปฏิทินการศึกษา', //ดู ดาวน์โหลด
              //         ),
              //         SideNavigationBarItem(
              //           icon: CustomIcons.graduation_cap_1, //Icons.grade,
              //           label: 'เกี่ยวกับการสำเร็จการศึกษา', //ผู้สำเร็จการศึกษา ตรวจสอบวันสำเร็จการศึกษา การยื่นขอสำเร็จการศึกษา
              //         ),
              //         SideNavigationBarItem(
              //           icon: CustomIcons.doc, //Icons.document_scanner,
              //           label: 'เอกสาร/ข้อมูลต่างๆ ที่เกี่ยวข้อง', //แบบฟอร์ม ค่าธรรมเนียม ระเบียบข้อบังคับ สถิตินิสิต ข้อมูลรายงานตัว สารสนเทศเจ้าหน้าที่
              //         ),
              //         SideNavigationBarItem(
              //           icon: CustomIcons.contacts, //Icons.contact_support,
              //           label: 'ติดต่อเรา', //ถามในระบบ ช่องทางอื่น
              //         ),
              //       ],
              //       onTap: (index) {
              //         setState(() {
              //           selectedIndex = index;
              //         });
              //       },
              //       theme: SideNavigationBarTheme(
              //         backgroundColor: Colors.brown[50],
              //         itemTheme: SideNavigationBarItemTheme(
              //             unselectedItemColor: Colors.brown[700],
              //             selectedItemColor: Colors.blueGrey,
              //             iconSize: 32.5,
              //             labelTextStyle: TextStyle(
              //                 fontSize: 20,
              //                 color: Colors.black
              //             )
              //         ),
              //         togglerTheme: SideNavigationBarTogglerTheme.standard(),
              //         dividerTheme: SideNavigationBarDividerTheme.standard(),
              //       ),
              //       toggler: SideBarToggler(
              //           expandIcon: Icons.keyboard_arrow_right,
              //           shrinkIcon: Icons.keyboard_arrow_left,
              //           onToggle: () {
              //             print('Toggle');
              //             setState(() {
              //               ++toggle;
              //             });
              //             print(toggle);
              //           }),
              //     ),
              //     /// Make it take the rest of the available width
              //     Flexible(
              //       // child: toggle % 2 != 0 ? views.elementAt(selectedIndex): Container(),
              //       child: views.elementAt(selectedIndex),
              //     )
              //   ],
              // )
          // )
        // )
      );
    }
}

//class _RegAppState extends State<RegApp> {
//
//   @override
//   Widget build(BuildContext context) {
//     return DevicePreview(
//         tools: const [
//           DeviceSection(),
//         ],
//         builder: (context) => MaterialApp(
//             debugShowCheckedModeBanner: false,
//             useInheritedMediaQuery: true,
//             builder: DevicePreview.appBuilder,
//             locale: DevicePreview.locale(context),
//             title: 'Responsive and adaptive UI in Flutter',
//             theme: ThemeData(
//                 primarySwatch: Colors.brown,
//                 textTheme: TextTheme(
//                   bodyText2: TextStyle(color: Colors.brown[900]),
//                   subtitle1: TextStyle(color: Colors.brown[900]),
//                 ),
//                 iconTheme: IconThemeData(
//                   color: Colors.brown[900],
//                 )
//
//             ),
//             home: Scaffold(
//                 backgroundColor: Colors.brown[50],
//                 appBar: buildAppBar(),
//                 body: buildBodyMainWidget(),
//                 drawer: Drawer(
//                   child: ListView(
//                   padding: EdgeInsets.zero,
//                   children: [
//                     const DrawerHeader(
//                       decoration: BoxDecoration(
//                         color: Colors.brown,
//                       ),
//                       child: Text('Drawer Header'),
//                     ),
//                     ListTile(
//                       title: const Text('Item 1'),
//                       onTap: () {
//                         // Update the state of the app
//                         // ...
//                         // Then close the drawer
//                         Navigator.pop(context);
//                       },
//                     ),
//                     ListTile(
//                       title: const Text('Item 2'),
//                         onTap: () {
//                         // Update the state of the app
//                         // ...
//                         // Then close the drawer
//                         Navigator.pop(context);
//                       },
//                     ),
//                   ],
//                   ),
//                 )
//             )
//         )
//     );
//   }
// }
