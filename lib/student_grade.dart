import 'package:flutter/material.dart';
import 'package:midterm_project/my_flutter_app_icons.dart';
import 'package:midterm_project/reg_fpage.dart';
import 'package:midterm_project/reg_mainpage.dart';
import 'package:midterm_project/student_regis.dart';

class StudentGrade extends StatefulWidget {
  const StudentGrade({super.key});

  @override
  State<StudentGrade> createState() => _StudentGradeState();
}

class _StudentGradeState extends State<StudentGrade> {
  Widget select = buildGradeWidget();
  String dropdownvalue = 'Item 1';
  // List of items in our dropdown menu
  List<String> items = [
    'Item 1',
    'Item 2',
    'Item 3',
    'Item 4',
    'Item 5',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBar(context),
        body: Container(
            padding: EdgeInsets.all(6),
            child:ListView(
              children: [
                buildDtaStdWidget(),
                Divider(
                  color: Colors.brown[700],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Row(
                          children: [
                            IconButton(
                              onPressed: (){
                                setState(() {
                                  select = buildGradeWidget();
                                });
                              },
                              icon: Icon(Icons.sticky_note_2_outlined),
                            ),
                            Text('ผลการศึกษา'),
                          ],
                        )
                        ,),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Row(
                          children: [
                            IconButton(
                              onPressed: (){
                                setState(() {
                                  select = buildGraduationWidget(dropdownvalue, items);
                                });
                              },
                              icon: Icon(CustomIcons.graduation_cap_1),
                            ),
                            Text('ตรวจสอบจบ'),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Divider(
                  color: Colors.brown[700],
                ),
                select,
              ],
            )
        )
    );
  }
}

AppBar buildAppBar(BuildContext context){
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.brown[700],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined, //login_outlined, //arrow_back_ios_new_outlined,
          color: Colors.white,
        )
    ),
    title: Text("Student Grade"),
    actions: <Widget>[
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.print,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.g_translate_outlined,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const RegF()),
            );
          },
          icon: Icon(
            Icons.power_settings_new, //logout_outlined,
            color: Colors.white,
          )
      ),
    ],
  );
}

Widget buildSelectYearWidget(){
  return Row(
    children: [
      Text('ปีการศึกษา:'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.arrow_left_rounded,
          )
      ),
      Text('2565', style: TextStyle( color: Colors.blueGrey[600], fontWeight: FontWeight.bold),),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.arrow_right_rounded,
          )
      ),
      Text('|'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.all_inclusive,
          )
      ),
      Text('ดูทั้งหมด'),
    ],
  );
}

Widget buildSelectDurationWidget(){
  return Row(
    children: [
      Icon(Icons.more_vert),
      Text('ภาคเรียน:'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.looks_one_outlined,
          )
      ),
      Text('/'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.looks_two_outlined,
            color: Colors.blueGrey,
          )
      ),
      Text('/'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.sunny,
          )
      ),
      Text('ภาคฤดูร้อน'),
    ],
  );
}

buildGradeWidget(){
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children:<Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ผลการศึกษา",textScaleFactor: 1.5,style: TextStyle(fontWeight:FontWeight.bold),),
        ),
        buildSelectYearWidget(),
        buildSelectDurationWidget(),
        Divider(
          color: Colors.brown[700],
        ),
        Table(
          columnWidths: const <int, TableColumnWidth>{
            0 : FlexColumnWidth(1),
            1 : FlexColumnWidth(3),
            2 : FlexColumnWidth(1),
            3: FlexColumnWidth(1),
          },
          defaultColumnWidth: const FlexColumnWidth(),
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: <TableRow>[
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[100],
              ),
              children: const <Widget>[
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: 'ภาคการศึกษาที่ 2/2565', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
              ],
            ),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                RegisSubjCell(title: 'รหัสวิชา', isHeader: true),
                RegisSubjCell(title: 'ชื่อรายวิชา', isHeader: true),
                RegisSubjCell(title: 'หน่วยกิต', isHeader: true),
                RegisSubjCell(title: 'เกรด', isHeader: true),
              ],
            ),
            ...RegisSubject.getRegisSubjects().map((regisSubj) {
              return TableRow(children: [
                RegisSubjCell(title: regisSubj.subjId),
                RegisSubjCell(title: regisSubj.subjName, isCenData : false),
                RegisSubjCell(title: '${regisSubj.subjUnit}'),
                RegisSubjCell(title: regisSubj.grade),
              ]);
            }),
          ],
        ),
        Table(
          columnWidths: const <int, TableColumnWidth>{
            0 : FlexColumnWidth(5),
            1 : FlexColumnWidth(5),
          },
          defaultColumnWidth: const FlexColumnWidth(),
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: <TableRow>[
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[100],
              ),
              children: const <Widget>[
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: 'ผลการศึกษา', isHeader: true),
              ],
            ),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[100],
              ),
              children: const <Widget>[
                RegisSubjCell(title: 'THIS SEMESTER', isHeader: true),
                RegisSubjCell(title: 'CUMULATIVE TO THIS SEMESTER', isHeader: true),
              ],
            ),
          ],
        ),
        Table(
          columnWidths: const <int, TableColumnWidth>{
            0 : FlexColumnWidth(3.5),
            1 : FlexColumnWidth(2.5),
            2 : FlexColumnWidth(1.5),
            3 : FlexColumnWidth(2),
            4 : FlexColumnWidth(2),
            5 : FlexColumnWidth(3.5),
            6 : FlexColumnWidth(2.5),
            7 : FlexColumnWidth(1.5),
            8 : FlexColumnWidth(2),
            9 : FlexColumnWidth(2),
          },
          defaultColumnWidth: const FlexColumnWidth(),
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: <TableRow>[
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                RegisSubjCell(title: 'C.Register\n18',),
                RegisSubjCell(title: 'C.Earn\n0',),
                RegisSubjCell(title: 'CA\n0',),
                RegisSubjCell(title: 'GP\n0',),
                RegisSubjCell(title: 'GPA\n0',),
                RegisSubjCell(title: 'C.Register\n112', isHeader: true),
                RegisSubjCell(title: 'C.Earn\n94', isHeader: true),
                RegisSubjCell(title: 'CA\n94', isHeader: true),
                RegisSubjCell(title: 'GP\n364', isHeader: true),
                RegisSubjCell(title: 'GPA\n3.87', isHeader: true),
              ],
            ),
          ],
        ),
        Text('\nการแสดงเกรด\n'
            '1. กรณีที่นิสิตประเมินครบทุกรายวิชา และเกรดออกแล้ว ช่องแสดงเกรดจะเป็นเกรดที่ได้\n'
            '2. กรณีที่นิสิตประเมินครบทุกวิชาแล้ว แต่เกรดยังไม่ออก ช่องแสดงเกรดจะเป็นช่องว่าง\n'
            '3. กรณีที่นิสิตประเมินไม่ครบทุกวิชา และเกรดออกแล้ว ช่องแสดงเกรดจะมีเครื่องหมาย ??',
          style: TextStyle(color: Colors.red[900]),),
        Text('\nหมายเหตุ\n'
            'CA= Credit Attempt, GP= Point , GPA =GP/CA Grade point average\n'
            'C.Register=หน่วยกิตลงทะเบียน , C.Earn=หน่วยกิตที่ได้รับ , CA=หน่วยกิตคำนวน ,GP=ค่าคะแนน ,GPA=ค่าคะแนนเฉลี่ย\n'
            'โปรแกรมทดสอบเกรด จะยังไม่นำผลของการ Regrade F ไปคำนวณจนกว่าจะได้รับเกรดจริง'),
      ]
  );
}

buildGraduationWidget(String dropdownvalue, items){
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children:<Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ตรวจสอบจบ",textScaleFactor: 1.5,style: TextStyle(fontWeight:FontWeight.bold),),
        ),
        DropdownButton<String>(
          hint: Text('แสดงข้อมูลสรุป', style: TextStyle(color: Colors.brown[900]),),
          items: <String>['แสดงข้อมูลสรุป', 'แสดงรายละเอียดทั้งหลักสูตร', 'แสดงรายละเอียดเฉพาะวิชาที่ลง'].map((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value, style: TextStyle(color: Colors.brown[900]),),
            );
          }).toList(),
          onChanged: (val) {
            // setState(() {
              dropdownvalue = val!;
            // });
          },
        ),
        Table(
          columnWidths: const <int, TableColumnWidth>{
            0 : FlexColumnWidth(3),
            1 : FlexColumnWidth(7),
          },
          defaultColumnWidth: const FlexColumnWidth(),
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: <TableRow>[
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[100],
              ),
              children: const <Widget>[
                GraduateChCell(title: 'โครงสร้างหลักสูตร', isHeader: true),
                GraduateChCell(title: '2115020: วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ', isHeader: true),
              ],
            ),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                GraduateChCell(title: 'ผลการอนุมัติ', isHeader: true),
                GraduateChCell(title: 'FAIL', isTailData: true, isRed: true,),
              ],
            ),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                GraduateChCell(title: 'หน่วยกิต', isHeader: true),
                GraduateChCell(title: 'หน่วยกิตขาด 38', isTailData: true, isRed: true,),
              ],
            ),
            TableRow(
              children: const <Widget>[
                GraduateChCell(title: 'หน่วยกิตต่ำสุด 132',),
                GraduateChCell(title: 'หน่วยกิตที่ลง: 112 | ผ่าน: 94 | รอ: 18(6)',),
              ],
            ),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                GraduateChCell(title: 'ปีศึกษา', isHeader: true),
                GraduateChCell(title: 'PASS', isTailData: true,),
              ],
            ),
            TableRow(
              children: const <Widget>[
                GraduateChCell(title: 'ปีสูงสุด 8',),
                GraduateChCell(title: 'ปีศึกษา: ปกติ 4 | ชั้นปี: 3',),
              ],
            ),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                GraduateChCell(title: 'GPA', isHeader: true),
                GraduateChCell(title: 'PASS', isTailData: true,),
              ],
            ),
            TableRow(
              children: const <Widget>[
                GraduateChCell(title: 'GPA ต่ำสุด 2.00',),
                GraduateChCell(title: 'GPA 3.87',),
              ],
            ),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                GraduateChCell(title: 'รายวิชาในคณะ', isHeader: true),
                GraduateChCell(title: '',),
              ],
            ),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                GraduateChCell(title: 'หน่วยกิตที่ลง 62',),
                GraduateChCell(title: 'หน่วยกิตที่ผ่าน: 62 | GPA: 3.81',),
              ],
            ),
          ],
        ),
        Padding(
            padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
            child: Divider(
              color: Colors.brown[700],
            ),
        ),
        Table(
          columnWidths: const <int, TableColumnWidth>{
            0 : FlexColumnWidth(2),
            1 : FlexColumnWidth(4),
            2 : FlexColumnWidth(2),
            3 : FlexColumnWidth(1.5),
            4 : FlexColumnWidth(1.5),
            5 : FlexColumnWidth(1.5),
            6 : FlexColumnWidth(1.5),
            7 : FlexColumnWidth(2),
            8 : FlexColumnWidth(2),
            9 : FlexColumnWidth(2),
          },
          defaultColumnWidth: const FlexColumnWidth(),
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: <TableRow>[
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[100],
              ),
              children: const <Widget>[
                RegisSubjCell(title: 'หมวด', isHeader: true),
                RegisSubjCell(title: 'คำอธิบาย', isHeader: true),
                RegisSubjCell(title: 'หน่วยกิต', isHeader: true),
                RegisSubjCell(title: 'CA', isHeader: true),
                RegisSubjCell(title: 'CS', isHeader: true),
                RegisSubjCell(title: 'CW', isHeader: true),
                RegisSubjCell(title: 'CG', isHeader: true),
                RegisSubjCell(title: 'PT', isHeader: true),
                RegisSubjCell(title: 'GPA', isHeader: true),
                RegisSubjCell(title: 'สถานภาพ', isHeader: true),
              ],
            ),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                RegisSubjCell(title: '1', isHeader: true),
                RegisSubjCell(title: 'หมวดวิชาศึกษาทั่วไป', isHeader: true),
                RegisSubjCell(title: '30', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
              ],
            ),
            ...GradSubj.getGradSubj1().map((regisSubj) {
              return TableRow(children: [
                GraduateChCell(title: regisSubj.id),
                GraduateChCell(title: regisSubj.descp),
                GraduateChCell(title: regisSubj.units, isTailData: true,),
                GraduateChCell(title: regisSubj.ca, isTailData: true,),
                GraduateChCell(title: regisSubj.cs, isTailData: true,),
                GraduateChCell(title: regisSubj.cw, isTailData: true,),
                GraduateChCell(title: regisSubj.cg, isTailData: true,),
                GraduateChCell(title: regisSubj.pt, isTailData: true,),
                GraduateChCell(title: regisSubj.gpa, isTailData: true,),
                GraduateChCell(title: regisSubj.status, isTailData: true,),
              ]);
            }),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                RegisSubjCell(title: '2', isHeader: true),
                RegisSubjCell(title: 'หมวดวิชาเฉพาะ ไม่น้อยกว่า', isHeader: true),
                RegisSubjCell(title: '96', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
              ],
            ),
            TableRow(
              children: const <Widget>[
                RegisSubjCell(title: '2.1',),
                RegisSubjCell(title: 'กลุ่มวิชาแกน',),
                RegisSubjCell(title: '18',),
                RegisSubjCell(title: '18',),
                RegisSubjCell(title: '18',),
                RegisSubjCell(title: '-',),
                RegisSubjCell(title: '18',),
                RegisSubjCell(title: '69',),
                RegisSubjCell(title: '3.83',),
                RegisSubjCell(title: 'PASS',),
              ],
            ),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                RegisSubjCell(title: '2.2', isHeader: true),
                RegisSubjCell(title: 'กลุ่มวิชาเฉพาะด้าน ไม่น้อยกว่า', isHeader: true),
                RegisSubjCell(title: '57', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
                RegisSubjCell(title: '', isHeader: true),
              ],
            ),
            ...GradSubj.getGradSubj2().map((regisSubj) {
              return TableRow(children: [
                GraduateChCell(title: regisSubj.id),
                GraduateChCell(title: regisSubj.descp),
                GraduateChCell(title: regisSubj.units, isTailData: true,),
                GraduateChCell(title: regisSubj.ca, isTailData: true,),
                GraduateChCell(title: regisSubj.cs, isTailData: true,),
                GraduateChCell(title: regisSubj.cw, isTailData: true,),
                GraduateChCell(title: regisSubj.cg, isTailData: true,),
                GraduateChCell(title: regisSubj.pt, isTailData: true,),
                GraduateChCell(title: regisSubj.gpa, isTailData: true,),
                GraduateChCell(title: regisSubj.status, isTailData: true,),
              ]);
            }),
            TableRow(
              decoration: BoxDecoration(
                color: Colors.brown[50],
              ),
              children: const <Widget>[
                RegisSubjCell(title: '3', isHeader: true),
                RegisSubjCell(title: 'หมวดเลือกเสรี ไม่น้อยกว่า', isHeader: true),
                RegisSubjCell(title: '6', isHeader: true),
                RegisSubjCell(title: '7', isHeader: true),
                RegisSubjCell(title: '7', isHeader: true),
                RegisSubjCell(title: '-', isHeader: true),
                RegisSubjCell(title: '7', isHeader: true),
                RegisSubjCell(title: '28', isHeader: true),
                RegisSubjCell(title: '4.00', isHeader: true),
                RegisSubjCell(title: 'PASS', isHeader: true),
              ],
            ),
          ],
        ),
        Text('\nหมายเหตุ\nCA= Credit Attempt,CS=Credit Satisfy, CW=Credit Wait,CG=Credit Point,PT=Point ,GPA=Grade Point Average'),
      ]
  );
}

class GraduateChCell extends StatelessWidget {
  final String title;
  final bool isHeader;
  final bool isCenData;
  final bool isTailData;
  final bool isRed;

  const GraduateChCell({
    Key? key,
    required this.title,
    this.isHeader = false,
    this.isCenData = true,
    this.isTailData = false,
    this.isRed = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isTailData ? Alignment.centerRight : Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
          style: TextStyle(
            fontWeight: isHeader ? FontWeight.bold : FontWeight.normal,
            color: isRed || this.title=='FAIL' ? Colors.red[900] : Colors.brown[900]),
        ),
      ),
    );
  }
}

class GradSubj {
  final String id;
  final String descp;
  final String units;
  final String ca;
  final String cs;
  final String cw;
  final String cg;
  final String pt;
  final String gpa;
  final String status;

  GradSubj(this.id, this.descp, this.units, this.ca, this.cs, this.cw, this.cg,
      this.pt, this.gpa, this.status);

  static List<GradSubj> getGradSubj1() {
    return [
      GradSubj('1.1', 'กลุ่มวิชาภาษาเพื่อการสื่อสาร', '12', '9', '9', '3', '9', '36', '4.00', 'FAIL'),
      GradSubj('1.2', 'กลุ่มวิชาอัตลักษณ์และคุณภาพชีวิตบัณฑิตบูรพา', '4', '4', '4', '-', '4', '16', '4.00', 'PASS'),
      GradSubj('1.3', 'กลุ่มวิชาทักษะชีวิตและความรับผิดชอบต่อสังคมและสิ่งแวดล้อม', '7', '7', '7', '-', '7', '28', '4.00', 'PASS'),
      GradSubj('1.4', 'กลุ่มวิชานวัตกรรมและความคิดสร้างสรรค์', '4', '4', '4', '-', '4', '16', '4.00', 'PASS'),
      GradSubj('1.5', 'กลุ่มวิชาเทคโนโลยีสารสนเทศ', '3', '3', '3', '-', '3', '12', '4.00', 'PASS'),
      ];
  }

  static List<GradSubj> getGradSubj2() {
    return [
      GradSubj('2.2.1', 'วิชาบังคับ ไม่น้อยกว่า', '36', '-', '33', '36', '-', '-', '3.73', 'FAIL'),
      GradSubj('2.2.1.1', 'กลุ่มประเด็นด้านองค์การและระบบสารสนเทศ', '2', '-', '-', '2', '-', '-', '-', 'FAIL'),
      GradSubj('2.2.1.2', 'กลุ่มเทคโนโลยีเพื่องานประยุกต์', '10', '9', '9', '1', '9', '36', '4.00', 'FAIL'),
      GradSubj('2.2.1.3', 'กลุ่มเทคโนโลยีและวิธีการทางซอฟต์แวร์', '15', '15', '15', '-', '15', '58.5', '3.90', 'PASS'),
      GradSubj('2.2.1.4', 'กลุ่มโครงสร้างพื้นฐานของระบบ', '6', '6', '6', '-', '6', '16.5', '2.75', 'PASS'),
      GradSubj('2.2.1.5', 'กลุ่มฮาร์ดแวร์และสถาปัตยกรรมคอมพิวเตอร์', '3', '3', '3', '-', '3', '12', '4.00', 'PASS'),
      GradSubj('2.2.2', 'วิชาเลือก (ให้เลือกเรียนกลุ่มใดกลุ่มหนึ่ง)', '15', '-', '6', '15', '-', '-', '4.00', 'FAIL'),
      GradSubj('2.2.2.1', 'กลุ่ม 1', '15', '6', '6', '9', '6', '24', '4.00', 'FAIL'),
      GradSubj('2.2.2.2', 'กลุ่ม 2', '15', '-', '-', '15', '-', '-', '-', 'FAIL'),
      GradSubj('2.2.3', 'วิชาสหกิจศึกษาหรือโครงงานคอมพิวเตอร์(หากเลือกโครงงาน ต้องเลือกเรียนจากหมวดเฉพาะเลือก อีก 3 หน่วยกิต)', '6', '-', '-', '6', '-', '-', '-', 'FAIL'),
      GradSubj('2.3', 'กลุ่มวิชาเฉพาะเลือก (เลือกเรียนกลุ่มใดกลุ่มหนึ่ง ไม่น้อยกว่า 12 หน่วยกิต)', '21', '-', '3', '21', '-', '-', '4.00', 'FAIL'),
      GradSubj('2.3.1', 'กลุ่ม 1', '12', '-', '-', '12', '-', '-', '-', 'FAIL'),
      GradSubj('2.3.2', 'กลุ่ม 2', '12', '-', '-', '12', '-', '-', '-', 'FAIL'),
      GradSubj('2.3.3', 'และให้เลือกเรียนไม่น้อยกว่า 9 หน่วยกิต จากกลุ่มเอกเลือกทั่วไปดังนี้', '9', '3', '3', '6', '3', '12', '4.00', 'FAIL'),
    ];
  }
}