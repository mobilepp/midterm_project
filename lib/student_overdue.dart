import 'package:flutter/material.dart';
import 'package:midterm_project/reg_fpage.dart';

class StudentOverdue extends StatelessWidget {
  const StudentOverdue({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBodyBillWidget(),
    );
  }
}

AppBar buildAppBar(BuildContext context){
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.brown[700],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined, //login_outlined, //arrow_back_ios_new_outlined,
          color: Colors.white,
        )
    ),
    title: Text("Student Overdue"),
    actions: <Widget>[
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.g_translate_outlined,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const RegF()),
            );
          },
          icon: Icon(
            Icons.power_settings_new, //logout_outlined,
            color: Colors.white,
          )
      ),
    ],
  );
}

Widget buildBodyBillWidget(){
  return Container(
      padding: EdgeInsets.all(16),
      child: ListView(
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          buildSummCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildSummRegCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildDomiCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildLibCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildOthOCardWidget(),
      ],
    ),
  );
}

buildSummCardWidget(){
  return Container(
    color: Colors.brown[50],
    child: Column(
      children: [
        Text(
          '\n"0 THB"\n',
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        ),
        Text(
          '  ค่าใช้จ่ายทั้งหมด  \n',
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        ),
      ],
    ),
  );
}

buildSummRegCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
              title: const Text(
                'ค่าลงทะเบียน',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '-- ไม่พบยอดเงินค้างชำระ --',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

buildDomiCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
              title: const Text(
                'ค่าเช่าหอพัก',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '-- ไม่พบยอดเงินค้างชำระ --',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

buildLibCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
              title: const Text(
                'ค่าปรับหอสมุด',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '-- ไม่พบยอดเงินค้างชำระ --',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

buildOthOCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
              title: const Text(
                'ค่าปรับอื่นๆ',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '--',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}
