import 'package:flutter/material.dart';

class SubjectRegister extends StatelessWidget {
  const SubjectRegister({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBodySubjWidget(),
    );
  }
}

Widget buildBodySubjWidget(){
  return Padding(
      padding: EdgeInsets.all(16.0),
    child: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("หลักสูตร/\nวิชาที่เปิดสอน",textScaleFactor: 2.5,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
          ),
          // Text('หลักสูตร/วิชาที่เปิดสอน //แนะนำการลงทะเบียน ค้นหา'),
          Row(
            children: [
              IconButton(
                onPressed: (){},
                icon: Icon(Icons.speaker_notes_rounded),
              ),
              Text('แนะนำการลงทะเบียน', textScaleFactor: 1.2,),
            ],
          ),
          Row(
            children: [
              Text('ขั้นที่ 1 หมวดวิชา', textScaleFactor: 1.1, style: TextStyle(fontWeight: FontWeight.bold)),
              Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: DropdownButton<String>(
                  hint: Text('รหัสวิชา ที่ยังใช้อยู่', style: TextStyle(color: Colors.brown[900]),),
                  items: <String>['รหัสวิชา ที่ยังใช้อยู่', 'ทุกรหัสวิชา', 'รหัสวิชา ทีเลิกใช้แล้ว',].map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value, style: TextStyle(color: Colors.brown[900]),),
                    );
                  }).toList(),
                  onChanged: (val) {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text('ขั้นที่ 2 หน่วยงานเจ้าของรายวิชา', textScaleFactor: 1.1, style: TextStyle(fontWeight: FontWeight.bold),),
              Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: DropdownButton<String>(
                  hint: Text('ทั้งหมด', style: TextStyle(color: Colors.brown[900]),),
                  items: <String>['ทั้งหมด', 'คณะวิทยาการสารสนเทศ', '...',].map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value, style: TextStyle(color: Colors.brown[900]),),
                    );
                  }).toList(),
                  onChanged: (val) {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text('ขั้นที่ 3 จำนวนรายการที่ได้จากการค้นหาไม่เกิน', textScaleFactor: 1.1, style: TextStyle(fontWeight: FontWeight.bold),),
              Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: DropdownButton<String>(
                  hint: Text('50', style: TextStyle(color: Colors.brown[900]),),
                  items: <String>['25', '50', '...',].map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value, style: TextStyle(color: Colors.brown[900]),),
                    );
                  }).toList(),
                  onChanged: (val) {},
                ),
              ),
            ],
          ),
          Text('ขั้นที่ 4', textScaleFactor: 1.1, style: TextStyle(fontWeight: FontWeight.bold),),
          buildSelectYearWidget(),
          buildSelectDurationWidget(),
          Row(
            children: [
              Text('วิทยาเขต', textScaleFactor: 1.1,),
              Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: DropdownButton<String>(
                  hint: Text('ทั้งหมด', style: TextStyle(color: Colors.brown[900]),),
                  items: <String>['ทั้งหมด', '1:บางแสน', '...',].map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value, style: TextStyle(color: Colors.brown[900]),),
                    );
                  }).toList(),
                  onChanged: (val) {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text('ระดับการศึกษา', textScaleFactor: 1.1,),
              Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: DropdownButton<String>(
                  hint: Text('ทั้งหมด', style: TextStyle(color: Colors.brown[900]),),
                  items: <String>['ทั้งหมด', '1:ปริญญาตรี ปกติ', '...',].map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value, style: TextStyle(color: Colors.brown[900]),),
                    );
                  }).toList(),
                  onChanged: (val) {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text('ประเภท', textScaleFactor: 1.1,),
              Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: DropdownButton<String>(
                  hint: Text('ทั้งหมด', style: TextStyle(color: Colors.brown[900]),),
                  items: <String>['ทั้งหมด', 'W:วิชาบังคับ', '...',].map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value, style: TextStyle(color: Colors.brown[900]),),
                    );
                  }).toList(),
                  onChanged: (val) {},
                ),
              ),
            ],
          ),
          Text('ขั้นที่ 5 ป้อนข้อความลงในช่องรหัสวิชาและ/หรือชื่อวิชาแล้วกดปุ่ม ค้นหาเพื่อเริ่มทำการค้นหาตามเงื่อนไข', textScaleFactor: 1.1, style: TextStyle(fontWeight: FontWeight.bold),),
          Container(
            padding: const EdgeInsets.all(10),
            child: TextField(
              autofocus: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'โปรดระบุรหัสวิชา',
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: TextField(
              autofocus: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'โปรดระบุชื่อวิชา',
              ),
            ),
          ),
          Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: ElevatedButton(
                child: const Text('ค้นหา'),
                onPressed: () {},
              )
          ),
          Text('*** กรุณาระบุรหัสวิชา หรือชื่อวิชาเพื่อใช้ในการค้นหา ***\nIdentify some part of the course code or course name and then use asterisk (*) for wildcard.', style: TextStyle(color: Colors.red[900]),),
          Text('ตัวอย่าง\n'
  '1. ค้นหาวิชาที่มีรหัสขึ้นต้นด้วย102 : ป้อน 102* ลงในช่องรหัสวิชา\n'
  '2. ค้นหาวิชาที่มีคำว่า world เป็นส่วนหนึ่งของชื่อวิชา : ป้อน *world* ลงในช่องชื่อวิชา\n'
  '3. ค้นหาวิชาที่มีชื่อวิชาลงท้ายด้วย finance : ป้อน *finance ลงในช่องชื่อวิชา\n'
  '4. แสดงรายวิชาของคณะวิศวกรรมศาสตร์ : เลือกหน่วยงานคณะวิศวกรรมศาสตร์\n'
  '5. ค้นหาวิชาที่มีรหัสขึ้นต้นด้วย102 และมีชื่อวิชาลงท้ายด้วย เบื้องต้น : ป้อน 102* ลงในช่องรหัสวิชา และป้อน *เบื้องต้น ลงในช่องชื่อวิชา\n'),
        ]
    ),
  );
}

Widget buildSelectYearWidget(){
  return Row(
    children: [
      Text('ปีการศึกษา:', textScaleFactor: 1.1),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.arrow_left_rounded,
          )
      ),
      Text('2565', style: TextStyle( color: Colors.blueGrey[600], fontWeight: FontWeight.bold),),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.arrow_right_rounded,
          )
      ),
    ],
  );
}

Widget buildSelectDurationWidget(){
  return Row(
    children: [
      Icon(Icons.more_vert),
      Text('ภาคเรียน:', textScaleFactor: 1.1),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.looks_one_outlined,
          )
      ),
      Text('/'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.looks_two_outlined,
            color: Colors.blueGrey,
          )
      ),
      Text('/'),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.sunny,
          )
      ),
      Text('ภาคฤดูร้อน'),
    ],
  );
}