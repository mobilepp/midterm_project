import 'package:flutter/material.dart';
import 'package:midterm_project/reg_fpage.dart';

class StudentProfile extends StatelessWidget {
  const StudentProfile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBodyProfileWidget(),
      //   child: ElevatedButton(
      //     onPressed: () {
      //       Navigator.pop(context);
      //     },
      //     child: const Text('Go back!'),
      //   ),

    );
  }
}

AppBar buildAppBar(BuildContext context){
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.brown[700],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined, //login_outlined, //arrow_back_ios_new_outlined,
          color: Colors.white,
        )
    ),
    title: Text("Student Profile"),
    actions: <Widget>[
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.print,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.g_translate_outlined,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const RegF()),
            );
          },
          icon: Icon(
            Icons.power_settings_new, //logout_outlined,
            color: Colors.white,
          )
      ),
    ],
  );
}

Widget buildBodyProfileWidget(){
  return Container(
      padding: EdgeInsets.all(16),
      child: ListView(
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          buildCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildAccountCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildStdCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildPersonalCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildOthPerCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildMoreDataCardWidget(),
          const SizedBox(
            height: 10,
          ),
        ],
      )
  ) ;
}

Widget buildCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 100,
                  height: 120,
                  child: Image(
                    image: AssetImage('images/mukku.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                Text('มหาวิทยาลัยบูรพา\n'
                    'Burapha University \n'
                    'คณะวิทยาการสารสนเทศ \n'
                    'Faculty of Informatics \n'
                    '63160066 \n'
                    'นางสาว จุฑามาศ ลืออริยทรัพย์ \n'
                    'MISS JUTAMAS LUEARIYASAP')
              ],
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildAccountCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
              title: const Text(
                'บัญชีผู้ใช้งาน',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              color: Colors.brown[50],
              child: Column(
                children: [
                  Text(
                    '\n"55 Days"\n',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    '  จำนวนวันที่ใช้รหัสผ่านได้  \n',
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ), //S
            Text(
                  'รหัสผ่านหมดอายุ: 18 มีนาคม 2566 [14:03:46]\n'
                  'บัญชีผู้ใช้หมดอายุ: 29 เมษายน 2573 [19:01:15]\n',
              style: TextStyle(color: Colors.red[900]),
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 144,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.brown[700])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('เปลี่ยนรหัสผ่าน'),
                      //Icon(Icons.touch_app),
                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildStdCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
              title: const Text(
                'ข้อมูลด้านการศึกษา',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
              // subtitle: Text(
              //   'Secondary Text',
              //   style: TextStyle(color: Colors.black.withOpacity(0.6)),
              // ),
            ),
            Text(
              '\nรหัสประจำตัว: 63160066\n'
                  'เลขที่บัตรประชาชน: 1209501132630\n'
                  'ชื่อ: นางสาวจุฑามาศ ลืออริยทรัพย์\n'
                  'ชื่ออังกฤษ: MISS JUTAMAS LUEARIYASAP\n'
                  'คณะ: คณะวิทยาการสารสนเทศ\n'
                  'วิทยาเขต: บางแสน\n'
                  'หลักสูตร: 2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ\n'
                  'วิชาโท: -\n'
                  'ระดับการศึกษา: ปริญญาตรี\n'
                  'ชื่อปริญญา: วิทยาศาสตรบัณฑิต วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ\n'
                  'ปีการศึกษาที่เข้า: 2563/1 (29/4/2563)\n'
                  'สถานภาพ: กำลังศึกษา\n'
                  'วิธีรับเข้า: โครงการภาคตะวันออก12จังหวัด\n'
                  'วุฒิก่อนเข้ารับการศึกษา: ม.6 (3.81)\n'
                  'จบการศึกษาจาก: จุฬาภรณราชวิทยาลัย ชลบุรี\n'
                  'อ.ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน\n',
            ),
          const SizedBox(
            height: 10,
          ), //SizedBox
          SizedBox(
            width: 128,
            child: ElevatedButton(
              onPressed: () => 'Null',
              style: ButtonStyle(
                  backgroundColor:
                  MaterialStateProperty.all(Colors.brown[700])),
              child: Padding(
                padding: const EdgeInsets.all(2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text('แก้ไขข้อมูล'),
                    //Icon(Icons.touch_app),
                    Icon(Icons.edit),
                  ],
                ),
              ),
            ),
          ),
            // ButtonBar(
            //   alignment: MainAxisAlignment.start,
            //   children: [
            //     FlatButton(
            //       textColor: const Color(0xFF6200EE),
            //       onPressed: () {
            //       },
            //       child: const Text('ACTION 1'),
            //     ),
            //     FlatButton(
            //       textColor: const Color(0xFF6200EE),
            //       onPressed: () {
            //       },
            //       child: const Text('ACTION 2'),
            //     ),
            //   ],
            // ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildPersonalCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
              title: const Text(
                    'ข้อมูลส่วนบุคคล',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
            ),
            Text(
              '\nสัญชาติ: ไทย ศาสนา: พุทธ\n'
                  'หมู่เลือด: B\n'
                  'ชื่อบิดา: นายธนพล ลืออริยทรัพย์\n'
                  'ชื่อมารดา: นางกนกพร ลืออริยทรัพย์\n'
                  'ที่อยู่: 2/14 ถนนสถาวร แขวง/ตำบล บ้านบึง เขต/อำเภอ บ้านบึง ชลบุรี 20170\n'
                  'โทร: 0625319822\n'
                  'ผู้ปกครอง: นายธนพล ลืออริยทรัพย์\n'
                  'ที่อยู่ (ผู้ปกครอง): 2/14 ถนนสถาวร แขวง/ตำบล บ้านบึง เขต/อำเภอ บ้านบึง ชลบุรี 20170\n'
                  'โทร: 0831159773\n',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 128,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.brown[700])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('แก้ไขข้อมูล'),
                      //Icon(Icons.touch_app),
                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildOthPerCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
              title: const Text(
                'บุคคลที่สามารถติดต่อได้',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '\nชื่อ: นางสาวดวงกมล ลืออริยทรัพย์\n'
                  'ที่อยู่: 2/14 ถนนสถาวร แขวง/ตำบล บ้านบึง เขต/อำเภอ บ้านบึง ชลบุรี 20170\n'
                  'โทร: 0625320628\n',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 128,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.brown[700])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('แก้ไขข้อมูล'),
                      //Icon(Icons.touch_app),
                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildMoreDataCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.circle_outlined, color: Colors.brown[700],),
              title: const Text(
                'ข้อมูลเพิ่มเติมอื่นๆ',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '\nความพิการ: ไม่พิการ'
                  'สถานภาพการรับทุน: ไม่ได้รับทุน'
                  'ความต้องการทุนการศึกษา: ต้องการ\n'
                  'จำนวนพี่น้องทั้งหมด (รวมตัวเอง): 3\n'
                  'นิสิตเป็นบุตรคนที่: 1\n'
                  'จำนวนพี่น้องที่กำลังศึกษาอยู่(รวมตัวเอง):3'
                  'สถานภาพของบิดา: มีชีวิต\n'
                  'รายได้บิดา: < 150,000 บาทต่อปี (< 12,500 บาทต่อเดือน)\n'
                  'อาชีพบิดา: เกษตร,ประมง\n'
                  'วุฒิการศึกษาสูงสุดของบิดา: ประถมศึกษา\n'
                  'สถานภาพของมารดา: มีชีวิต\n'
                  'รายได้มารดา: ไม่ระบุ\n'
                  'อาชีพมารดา: ค้าขาย,ธุรกิจส่วนตัวและอาชีพอิสระ/รับจ้างอิสระแบบไม่ประจำ\n'
                  'วุฒิการศึกษาสูงสุดของมารดา: ประถมศึกษา\n'
                  'สภาพการสมรสของบิดามารดา: อยู่ด้วยกัน\n'
                  'รายได้ผู้ปกครอง: < 150,000 บาทต่อปี (< 12,500 บาทต่อเดือน)\n'
                  'อาชีพผู้ปกครอง: เกษตร,ประมง\n',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 128,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.brown[700])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('แก้ไขข้อมูล'),
                      //Icon(Icons.touch_app),
                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}