import 'package:flutter/material.dart';
import 'package:midterm_project/reg_mainpage.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body:
      // Center(
      //   child: buildBodyLoginWidget(context),
      // ),
      Padding(
          padding: const EdgeInsets.all(60),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Container(
                child: Image(
                  image: AssetImage('images/buu_logo.png'),
                  height: 150, width: 150,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                  autofocus: true,
                  controller: nameController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'User Name',
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: TextField(autofocus: true,
                  obscureText: true,
                  controller: passwordController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextButton(
                onPressed: () {
                  //forgot password screen
                },
                child: const Text('Forgot Password?',),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  height: 50,
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: ElevatedButton(
                    child: const Text('Login'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const Reg()),
                      );
                      print(nameController.text);
                      print(passwordController.text);
                    },
                  )
              ),
            ],
          )
      )
    );
  }
}

AppBar buildAppBar(BuildContext context){
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.brown[700],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined, //login_outlined, //arrow_back_ios_new_outlined,
          color: Colors.white,
        )
    ),
    title: Text("Reg BUU Login"),
    actions: <Widget>[
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.g_translate_outlined,
            color: Colors.white,
          )
      ),
    ],
  );
}

// buildBodyLoginWidget(context){
//   return ElevatedButton(
//     onPressed: () {
//       Navigator.push(
//         context,
//         MaterialPageRoute(builder: (context) => const Reg()),
//       );
//     },
//     child: const Text('Login!'),
//   );
// }