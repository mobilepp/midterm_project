import 'package:flutter/material.dart';
import 'package:midterm_project/student_grade.dart';

class SubjectRegister extends StatelessWidget {
  const SubjectRegister({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBodyDocGradeWidget(),
    );
  }
}

Widget buildBodyDocGradeWidget(){
  return ListView(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("การสำเร็จการศึกษา",textScaleFactor: 2.5,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
      ),
      // Text('การสำเร็จการศึกษา //ผู้สำเร็จการศึกษา ตรวจสอบวันสำเร็จการศึกษา การยื่นขอสำเร็จการศึกษา'),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: const Steps(),
      ),
    ]
  );
}

class Step {
  Step(
      this.title,
      this.body,
      [this.isExpanded = false]
      );
  String title;
  Widget body;
  bool isExpanded;
}

List<Step> getSteps() {
  return [
    Step('ตรวจสอบจบ', getGraduateCheckData()),
    Step('ผู้สำเร็จการศึกษา', getGraduateStdData()),
    Step('การยื่นขอสำเร็จการศึกษา', getGraduateDocData()),
  ];
}

Widget getGraduateStdData(){
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("จบการศึกษา",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
      ),
      Container(
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            Text('คณะ', textScaleFactor: 1.1, style: TextStyle(fontWeight: FontWeight.bold),),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: DropdownButton<String>(
                hint: Text('34:คณะวิทยาการสารสนเทศ', style: TextStyle(color: Colors.brown[900]),),
                items: <String>['...', '34:คณะวิทยาการสารสนเทศ', '...'].map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value, style: TextStyle(color: Colors.brown[900]),),
                  );
                }).toList(),
                onChanged: (val) {},
              ),
            ),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("ปีการศึกษา : 2546 2547 2548 2549 2550 2551 2552 2553 2554 2555 2556 2557 2558 2559 2560 2561 2562 2563 2564 [2565] 2566 2567",textScaleFactor: 1.1,style: TextStyle(fontWeight:FontWeight.bold)),
      ),
      Table(
        columnWidths: const <int, TableColumnWidth>{
          0 : FlexColumnWidth(4),
          1 : FlexColumnWidth(1),
          2 : FlexColumnWidth(1),
          3 : FlexColumnWidth(1.5),
        },
        defaultColumnWidth: const FlexColumnWidth(),
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: <TableRow>[
          TableRow(
            children: const <Widget>[
              GraduateChCell(title: 'ภาคการศึกษาที่', isTailData: true),
              GraduateChCell(title: '1', isHeader: true),
              GraduateChCell(title: '2', isHeader: true),
              GraduateChCell(title: 'Summer', isHeader: true),
            ],
          ),
          TableRow(
            decoration: BoxDecoration(
              color: Colors.brown[100],
            ),
            children: const <Widget>[
              GraduateChCell(title: 'วิทยาเขต : บางแสน', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
            ],
          ),
          TableRow(
            decoration: BoxDecoration(
              color: Colors.brown[50],
            ),
            children: const <Widget>[
              GraduateChCell(title: 'ระดับการศึกษา : ปริญญาตรี ปกติ', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
            ],
          ),
          TableRow(
            children: const <Widget>[
              GraduateChCell(title: '2115010 : วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 54 - ป.ตรี 4 ปี ปกติ'),
              GraduateChCell(title: '1', isHeader: true),
              GraduateChCell(title: '-'),
              GraduateChCell(title: '-'),
            ],
          ),
          TableRow(
            children: const <Widget>[
              GraduateChCell(title: '2115012 : วท.บ. (เทคโนโลยีสารสนเทศ) ใหม่ 54 - ป.ตรี 4 ปี ปกติ'),
              GraduateChCell(title: '-'),
              GraduateChCell(title: '1', isHeader: true),
              GraduateChCell(title: '-'),
            ],
          ),
          TableRow(
            children: const <Widget>[
              GraduateChCell(title: '2115020 : วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ'),
              GraduateChCell(title: '19', isHeader: true),
              GraduateChCell(title: '65', isHeader: true),
              GraduateChCell(title: '-'),
            ],
          ),
          TableRow(
            children: const <Widget>[
              GraduateChCell(title: '2115021 : วท.บ. (วิศวกรรมซอฟต์แวร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ'),
              GraduateChCell(title: '5', isHeader: true),
              GraduateChCell(title: '75', isHeader: true),
              GraduateChCell(title: '-'),
            ],
          ),
          TableRow(
            children: const <Widget>[
              GraduateChCell(title: '2115022 : วท.บ. (เทคโนโลยีสารสนเทศ) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ'),
              GraduateChCell(title: '4', isHeader: true),
              GraduateChCell(title: '83', isHeader: true),
              GraduateChCell(title: '-'),
            ],
          ),
          TableRow(
            decoration: BoxDecoration(
              color: Colors.brown[50],
            ),
            children: const <Widget>[
              GraduateChCell(title: 'ระดับการศึกษา : ปริญญาตรี พิเศษ', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
            ],
          ),
          TableRow(
            children: const <Widget>[
              GraduateChCell(title: '2515011 : วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 54 - 4 ปี พิเศษ'),
              GraduateChCell(title: '-'),
              GraduateChCell(title: '1', isHeader: true),
              GraduateChCell(title: '-'),
            ],
          ),
          TableRow(
            decoration: BoxDecoration(
              color: Colors.brown[50],
            ),
            children: const <Widget>[
              GraduateChCell(title: 'ระดับการศึกษา : ปริญญาโท เต็มเวลา', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
              GraduateChCell(title: '', isHeader: true),
            ],
          ),
          TableRow(
            children: const <Widget>[
              GraduateChCell(title: '4034206 : วท.ม. (วิทยาการข้อมูล) ปรับปรุง62-แบบ ก2 เต็มเวลา'),
              GraduateChCell(title: '2', isHeader: true),
              GraduateChCell(title: '-'),
              GraduateChCell(title: '-'),
            ],
          ),
       ],
      ),
    ],
  );
}

Widget getGraduateCheckData(){
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("ตรวจสอบจบ",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
      ),
      Container(
        padding: const EdgeInsets.all(10),
        child: TextField(
          autofocus: true,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'โปรดระบุเลขประจำตัวนิสิต',
          ),
        ),
      ),
      Container(
        padding: const EdgeInsets.all(10),
        child: TextField(
          autofocus: true,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'ชื่อนิสิต',
          ),
        ),
      ),
      Container(
        padding: const EdgeInsets.all(10),
        child: TextField(
          autofocus: true,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'นามสกุลนิสิต',
          ),
        ),
      ),
      Container(
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            Text('สถานภาพ', textScaleFactor: 1.1,),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: DropdownButton<String>(
                hint: Text('ทั้งหมด', style: TextStyle(color: Colors.brown[900]),),
                items: <String>['ทั้งหมด', 'กำลังศึกษา', 'จบการศึกษา', 'พ้นสภาพ'].map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value, style: TextStyle(color: Colors.brown[900]),),
                  );
                }).toList(),
                onChanged: (val) {},
              ),
            ),
          ],
        ),
      ),
      Container(
          height: 50,
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
          child: ElevatedButton(
            child: const Text('ค้นหา'),
            onPressed: () {},
          )
      ),
      Text('คำแนะนำ\n'
          '1. ถ้าต้องการค้นหานิสิตที่มีเลขประจำตัวขึ้นต้นด้วย 41 ให้ป้อน 41*\n'
          '2. ถ้าต้องการค้นหานิสิตที่มีชื่อขึ้นต้นด้วย สม ให้ป้อน สม*\n'
          '3. ถ้าต้องการค้นหานิสิตที่มีชื่อลงท้ายด้วย ชาย ให้ป้อน *ชาย\n'
          '4. ระบุสถานภาพของนิสิต\n'
          '5. กดปุ่ม ค้นหา เพื่อเริ่มทำการค้นหาตามเงื่อนไข\n'),
    ],
  );
}

Widget getGraduateDocData(){
  return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ประกาศมหาวิทยาลัยบูรพา\nการยื่นคำร้องขอสำเร็จการศึกษา",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
        ),
      ]
  );
}


class Steps extends StatefulWidget {
  const Steps({Key? key}) : super(key: key);
  @override
  State<Steps> createState() => _StepsState();
}

class _StepsState extends State<Steps> {
  final List<Step> _steps = getSteps();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: _renderSteps(),
      ),
    );
  }
  Widget _renderSteps() {
    return ExpansionPanelList(
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          _steps[index].isExpanded = !isExpanded;
        });
      },
      children: _steps.map<ExpansionPanel>((Step step) {
        return ExpansionPanel(
          backgroundColor: step.isExpanded ? Colors.white : Colors.brown.shade50,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return ListTile(
              title: Text(step.title, style: TextStyle(fontWeight: FontWeight.bold,),textScaleFactor: 1.2,),
            );
          },
          body: step.body,
          isExpanded: step.isExpanded,
        );
      }).toList(),
    );
  }
}