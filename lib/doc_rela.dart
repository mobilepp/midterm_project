import 'package:flutter/material.dart';

class SubjectRegister extends StatelessWidget {
  const SubjectRegister({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBodyDocRelaWidget(),
    );
  }
}

Widget buildBodyDocRelaWidget(){
  return ListView(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("เอกสาร/ข้อมูลต่างๆ ที่เกี่ยวข้อง",textScaleFactor: 2.5,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
      ),
      // Text('เอกสาร/ข้อมูลต่างๆ ที่เกี่ยวข้อง //แบบฟอร์ม ค่าธรรมเนียม ระเบียบข้อบังคับ สถิตินิสิต ข้อมูลรายงานตัว สารสนเทศเจ้าหน้าที่'),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: const Steps(),
      ),
    ]
  );
}

class Step {
  Step(
      this.title,
      this.body,
      [this.isExpanded = false]
      );
  String title;
  Widget body;
  bool isExpanded;
}

List<Step> getSteps() {
  return [
    Step('แบบฟอร์มคำร้อง', getRequestFormData()),
    Step('ค่าธรรมเนียมการศึกษา', getTuitionFeesData()),
    Step('ระเบียบ/ข้อบังคับมหาวิทยาลัย', getRuleUData()),
    Step('สถิตินิสิต', getStatStdData()),
    Step('ข้อมูลรายงานตัว', getAttendanceData()),
    Step('สารสนเทศเจ้าหน้าที่', getInformStaffData()),
  ];
}

Widget getAttendanceData(){
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("ตรวจสอบค่าธรรมเนียมที่ต้องชำระในการรายงานตัว (ไม่รวมค่าหอพัก)",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
      ),
      Container(
        padding: const EdgeInsets.all(10),
        child: TextField(
          autofocus: true,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'โปรดระบุเลขประจำตัวนิสิต',
          ),
        ),
      ),
      Container(
          height: 50,
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
          child: ElevatedButton(
            child: const Text('ค้นหา'),
            onPressed: () {},
          )
      ),
    ],
  );
}

Widget getRequestFormData(){
  return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ดาว์นโหลดแบบคำร้อง (Download Request form)",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
        ),
        Padding(
            padding: const EdgeInsets.all(8.0),
          child: Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              border: TableBorder.all(
                  color: Colors.brown.shade200,
                  style: BorderStyle.solid,
                  width: 2),
              columnWidths: {
                0: FlexColumnWidth(0.5),
                1: FlexColumnWidth(4),
              },
              children: [
                TableRow(
                    children: [
                      Column(children:[Text("ที่",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
                      Column(children:[Text("แบบคำร้อง (Request form)",textScaleFactor: 0.9, style: TextStyle(fontWeight: FontWeight.bold),)]),
                    ]
                ),
                TableRow(
                    children: [
                      Column(children:[Text("1",textScaleFactor: 0.9)]),
                      Column(crossAxisAlignment: CrossAxisAlignment.center,
                          children:[
                            Row(children: [
                              Padding(padding: EdgeInsets.all(6.0), child: Text("RE01 คำร้องทั่วไป (general form)",textScaleFactor: 0.9)),
                                  Icon(Icons.file_copy), Icon(Icons.file_download)]),
                            ]),
                    ]
                ),
                TableRow(
                    children: [
                      Column(children:[Text("2",textScaleFactor: 0.9)]),
                      Column(crossAxisAlignment: CrossAxisAlignment.center,
                          children:[
                            Row(children: [
                              Padding(padding: EdgeInsets.all(6.0), child: Text("RE02 คำร้องขอเงินค่าหน่วยกิตคืน",textScaleFactor: 0.9),),
                              Icon(Icons.file_copy), Icon(Icons.file_download)],)
                      ]),
                    ]
                ),
                TableRow(
                    children: [
                      Column(children:[Text("..",textScaleFactor: 0.9)]),
                      Column(crossAxisAlignment: CrossAxisAlignment.center,
                          children:[
                            Row(children: [
                              Padding(padding: EdgeInsets.all(6.0), child: Text("..",textScaleFactor: 0.9),),
                              Icon(Icons.file_copy), Icon(Icons.file_download)],)
                          ]),
                    ]
                ),
              ]
          ),
        )
    ]
  );
}

Widget getRuleUData(){
  return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ระเบียบ - ข้อบังคับมหาวิทยาลัย",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
        ),
      ]
  );
}

Widget getStatStdData(){
  return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("สถิติ TCAS (ปีการศึกษา 2565)",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
        ),
      ]
  );
}

Widget getTuitionFeesData(){
  return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ประกาศมหาวิทยาลัยบูรพา",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
        ),
      ]
  );
}

Widget getInformStaffData(){
  return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("ดาวน์โหลดประกาศต่างๆ",textScaleFactor: 1.2,style: TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
        ),
      ]
  );
}

class Steps extends StatefulWidget {
  const Steps({Key? key}) : super(key: key);
  @override
  State<Steps> createState() => _StepsState();
}

class _StepsState extends State<Steps> {
  final List<Step> _steps = getSteps();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: _renderSteps(),
      ),
    );
  }
  Widget _renderSteps() {
    return ExpansionPanelList(
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          _steps[index].isExpanded = !isExpanded;
        });
      },
      children: _steps.map<ExpansionPanel>((Step step) {
        return ExpansionPanel(
            backgroundColor: step.isExpanded ? Colors.white : Colors.brown.shade50,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return ListTile(
              title: Text(step.title, style: TextStyle(fontWeight: FontWeight.bold,),textScaleFactor: 1.1,),
            );
          },
          body: step.body,
          isExpanded: step.isExpanded,
        );
      }).toList(),
    );
  }
}